package oolala.controller;
/**
 * When prompted by the controller, this method returns the message of the most recent error
 * that was thrown
 * @return String, error message
 */
public class ErrorHandler {

  private String errorMessage;
  private boolean errorFound = false;


  protected String getErrorMessage() {
    return errorMessage;
  }

  /**
   * The controller is always checking to see if an error has been found, whenever the
   * boolean is set as true, the controller will call this method, catch the error, and then
   * display using the getErrorMessage()
   * @return boolean as to whether or not an error is found
   */
  public boolean isErrorFound() {
    return errorFound;
  }

  /**
   * Everytime an error is found in anything within the controller package (translator, generator). This method
   * will set a boolean to true signalling that an error exists. The method that threw the error will also pass
   * a message, so this is set as the error message.
   * @param message: String error message passed in from a method that encountered an error
   */
  public void setErrorFound(String message) {
    errorFound = true;
    errorMessage = message;
  }

  /**
   * This method clears the error after it has been thrown so that new errors can be found.
   */
  public void setErrorHandled() {
    errorFound = false;
    errorMessage = "";
  }

}
