package oolala.controller;

import oolala.commands.Command;
import oolala.controller.translator.CommandTranslator;
import oolala.controller.translator.DarwinTranslator;
import oolala.controller.translator.LSystemTranslator;
import oolala.controller.translator.LogoTranslator;
import oolala.model.AnimalData;
import oolala.model.DarwinManager;
import oolala.model.DrawingManager;
import oolala.model.GameModel;
import oolala.view.GameView;


import java.util.*;
/**
 * The GameController class acts as the brain behind the organization of the oolala game. It
 * receives input from the view, processes it, sends it to the model, and then sends the results
 * back to the view to display. It contains the main execution loop in the game, which is step().
 * It is useful in connecting the front end to the back end. We assumed that there are both fully
 * functioning model and view components that can be "connected" to the controller in order for
 * this game to run. We also assumed that the translator and command generator classes within the
 * controller would be functional. That being said, the controller is dependent on all of these
 * components being initialized and functional in order to do its job correctly. An example of how
 * to use it would be to create a new Controller, and "connect" it to a Model and View. This would
 * allow it to manage these two components for different oolala models and views.
 */
public class GameController {

  public static final String COMMAND_TRANSLATOR_CHOICE_BUNDLE = "oolala.controller.resources.CommandTranslatorChoices";
  public static final String ERROR_RESOURCE_BUNDLE = "oolala.controller.resources.Errors";
  public static final int NUM_LSYSTEM_PARAMETERS = 3;
  public static final int NUM_HOME_PARAMETERS = 2;

  private GameView myView;
  private final GameModel myModel;
  private final CommandGenerator myCommandGenerator;
  private CommandTranslator myCommandTranslator;
  private ResourceBundle myErrorsBundle;

  /**
   * Constructor for the GameController. Takes in a given model and view, and sets it as the model
   * and view to use for a given game. Very strongly depends on a functional model and view being
   * set. Additionally, sets a command generator for the game.
   *
   * @param view  the GUI of the game that the user can see and interact with
   * @param model the backend of the game that manages the animals and the game behavior
   */
  public GameController(GameView view, GameModel model) {
    myView = view;
    myModel = model;
    myCommandGenerator = new CommandGenerator();
  }

  /**
   * Sets the resource bundle that contains error messages that may need to be set during controller
   * behavior (in a given language). Assumes a valid resource file is provided that matches the
   * error resource bundle specified.
   *
   * @param language language to be used in the game
   */
  public void setResources(String language) {
    myErrorsBundle = ResourceBundle.getBundle(
        String.format("%s%s", ERROR_RESOURCE_BUNDLE, language));
    myCommandGenerator.setMyErrorsBundle(myErrorsBundle);
  }

  /**
   * The main game loop that is called by the animation. It displays data, executes a step, and then
   * displays the new data. We assume that the model is functional and executeStep() actually
   * completes a correct step execution.
   */
  public void step() {
    showInView();
    myModel.executeStep();
    showInView();
  }

  /**
   * Loops through all the animal data in the model and displays it in the model. Assumes that there
   * is valid data to display in the model and that the view can appropriately display this data.
   */

  public void showInView() {
    myModel.updateDataMap();
    for (AnimalData data : myModel.getAllData()) {
      myView.update(data);
    }
  }

  /**
   * Initializes animals of a given species for a game by calling setupAnimals with a given species
   * in the model. Assumes that there is a valid species input and that the model is equipped to set
   * up Animals.
   *
   * @param species type of animal to set up
   */

  public void setupAnimals(String species) {
    myModel.setupAnimals(species);

  }

  /**
   * Sets up the game with the appropriate command translator and animal managers for that type of
   * game. Also, for LSystem, this method reads in and parses parameters (length, etc.) that users
   * have inputted to set up the translator. Assumes that valid parameters have been passed, and
   * assumes that the user selects on of the three application choices.
   *
   * @param selectedGame the user's selected gam
   */

  public void setMyClassesForGameType(String selectedGame) {
    ResourceBundle myApplicationChoice = ResourceBundle.getBundle(COMMAND_TRANSLATOR_CHOICE_BUNDLE);
    try {
      if (selectedGame.equals(myApplicationChoice.getString("Logo"))) {
        myCommandTranslator = new LogoTranslator();
        myModel.setMyAnimalManager(new DrawingManager());
      } else if (selectedGame.equals(myApplicationChoice.getString("LSystem"))) {
        String parameters = myView.setUpSystem();
        List<String> parsedParameters = parseUserInput(parameters, NUM_LSYSTEM_PARAMETERS);
        myCommandTranslator = new LSystemTranslator(parsedParameters.get(0),
                parsedParameters.get(1), parsedParameters.get(2));
        myModel.setMyAnimalManager(new DrawingManager());
      } else if (selectedGame.equals(myApplicationChoice.getString("Darwin"))) {
        myCommandTranslator = new DarwinTranslator();
        myModel.setMyAnimalManager(new DarwinManager());
      }
      myCommandTranslator.setMyErrorsBundle(myErrorsBundle);
    }
    catch (Exception e){
        myView.displayError(myErrorsBundle.getString("InvalidOriginalParams"));
      }
    }


  /**
   * Changes the radius that the AnimalManager uses to detect animal proximity. Assumes a valid
   * radius is passed.
   *
   * @param radius the given radius specified by the user
   */
  public void animalRadiusChange(double radius) {
    myModel.setNewRadius(radius);
  }



  private List<String> parseUserInput(String parameters, int numExpectedParams) {
    List<String> parsedValues = new ArrayList<>();
    String[] applicationParameters = parameters.split(",");
    for (int i = 0; i < numExpectedParams; i++) {
      try {
        parsedValues.add(applicationParameters[i].trim());
      } catch (NullPointerException e) {
        myView.displayError(myErrorsBundle.getString("InvalidOriginalParams"));
        parsedValues.clear();
        return parseUserInput(myView.setUpSystem(), numExpectedParams);
      }
      catch(ArrayIndexOutOfBoundsException e){

      }
    }
    return parsedValues;
  }

  /**
   * Takes in a file and converts the text in that file to logo/basic instructions that can be used
   * to generate specific command objects. Assumes a valid file is passed and that the instructions
   * contained in the file are valid for the game the user has selected. Also handles any errors
   * found during parsing.
   *
   * @param filename the name of the instruction file the user is inputting
   * @return the list of translated logo instructions that the command translator produced
   */
  public List<String> createListOfIsnsFromFile(String filename) {
    List<String> translatedCommands = myCommandTranslator.translateIsnsFromFile(filename);
    checkForAndDisplayTranslatorErrors();
    return translatedCommands;
  }

  /**
   * Takes in a list of strings of instructions and converts that to logo/basic instructions that
   * can be used to generate specific command objects. Assumes valid instructions for the game a
   * user is playing are passed in the list. Also handles any errors found during parsing.
   *
   * @param addedCommands list of instructions for the game a user is playing
   * @return the list of translated logo instructions that the command translator produced
   */

  public List<String> createListOfIsnsFromInput(List<String> addedCommands) {
    List<String> translatedCommands = myCommandTranslator.translateFromLinesToIsns(addedCommands);
    checkForAndDisplayTranslatorErrors();
    return translatedCommands;
  }

  private void checkForAndDisplayTranslatorErrors() {
    ErrorHandler curErrors = myCommandTranslator.getMyErrorHandler();
    checkForAndDisplayErrors(curErrors);
  }

  /**
   * Creates Commands from split up logo instructions using the CommandGenerator, and gives these
   * Command objects to model to send to AnimalManager/Animals. Assumes that correctly parsed input
   * instructions have been passed as inputs. Displays any errors caught after generating Command
   * objects.
   *
   * @param inputInstructions input logo instructions to create into commands
   */

  public void createCommands(List<String> inputInstructions) {
    Queue<Command> commandsQueue = myCommandGenerator.createCommands(inputInstructions);
    ErrorHandler curErrors = myCommandGenerator.getMyErrorHandler();
    checkForAndDisplayErrors(curErrors);
    myModel.addCommands(commandsQueue);
  }

  private void checkForAndDisplayErrors(ErrorHandler curErrors) {
    if (curErrors.isErrorFound()) {
      myView.displayError(curErrors.getErrorMessage());
      curErrors.setErrorHandled();
    }
  }

  /**
   * Resets scene to default setting by clearing out model. Assumes that the model exists and can be
   * cleared out. Also assumes that this is only called when a new Game is requested (after one has
   * already been started).
   */

  public void resetScene() {
    myModel.clearOutModel();
  }

  /**
   * Updates home position for each animal in the game by parsing user inputs and instructing the
   * model to set new home positions for all animals. Assume that the user has passed valid inputs
   * for the home instructions.
   *
   * @param newHomeValues new parameters for home position
   */

  public void setHomeWithNewValues(String newHomeValues) {
    try {
      List<String> newXandY = parseUserInput(newHomeValues, NUM_HOME_PARAMETERS);
      myModel.getMyAnimalManager().setNewHomeForAnimals(Double.parseDouble(newXandY.get(0)),
              Double.parseDouble(newXandY.get(1)));
    }
    catch(NumberFormatException e){
      myView.displayError(myErrorsBundle.getString("InvalidHome"));
    }
  }

  protected CommandTranslator getMyCommandTranslator() {
    return myCommandTranslator;
  }
}
