package oolala.controller.translator;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import oolala.controller.ErrorHandler;

/**
 * Class that creates individual logo commands from a given string or file. This is an abstract
 * class, so it can be extended to translate whatever type of input commands given into specified
 * logo/basic commands that correspond to command objects. This class could fail if it is given
 * instructions that it does not recognize, or invalid files. It needs to be used with a
 * controller, that can pass it files or lists of instructions to translate. That being said, it
 * is dependent on a controller-type class, as well as command generator that can make its
 * logo/basic instructions into command objects. An example of using this would be to declare a
 * new LSystem translator, for example, and then call translateLinesIntoLogoIsns and pass a list
 * of LSystem Commands. It will return those commands as logo commands.
 */
public abstract class CommandTranslator {

  private final ErrorHandler myErrorHandler;
  private ResourceBundle myErrorsBundle;

  /**
   * Constructor for a CommandTranslator, creates a new CommandTranslator object. Assume that it has
   * an error handler class in the same package.
   */
  public CommandTranslator() {
    myErrorHandler = new ErrorHandler();
  }

  /**
   * Sets the ResourceBundle to use for errors to be displayed. Assumes that there is a valid
   * ResourceFile available to be passed as a parameter.
   *
   * @param errorsBundle ResourceBundle passed to set for myErrorsBundle
   */
  public void setMyErrorsBundle(ResourceBundle errorsBundle) {
    myErrorsBundle = errorsBundle;
  }


  protected List<String> translateLinesIntoLogoIsns(List<String> cmdsList) {
    return cmdsList;
  }


  /**
   * Translates a file of instructions into logo instructions. Assumes that a valid file is passed,
   * and that we want to get out individual logo/basic instructions.
   *
   * @param filename given file of instructions to translate into logo instructions
   * @return list of individual logo instructions
   */
  public List<String> translateIsnsFromFile(String filename) {
    List<String> cmdsList = new ArrayList<>();
    loadFileIntoLines(filename, cmdsList);
    return translateFromLinesToIsns(cmdsList);
  }

  /**
   * Translate a given list of strings of instructions into individual logo instructions. Assumes a
   * valid list of instructions is passed, and that you want to get split up logo instructions out.
   *
   * @param cmdsList list of instructions to translate
   * @return list of individual logo instructions
   */

  public List<String> translateFromLinesToIsns(List<String> cmdsList) {
    cmdsList = translateLinesIntoLogoIsns(cmdsList);
    return createListOfIndividualLogoIsns(cmdsList);

  }

  private void loadFileIntoLines(String filename, List<String> cmdsList) {
    try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
      String line = reader.readLine();
      while (line != null) {
        if (!line.startsWith("#") && !line.equals("")) {
          line = line.toLowerCase();
          cmdsList.add(line.trim());
        }
        line = reader.readLine();
      }
    } catch (IOException e) {
      myErrorHandler.setErrorFound(myErrorsBundle.getString("InvalidFile"));
    }
  }

  private List<String> createListOfIndividualLogoIsns(List<String> cmdsList) {
    List<String> cmdsParsed = new ArrayList<>();
    Pattern regExPattern = Pattern.compile("\\w+\\s*\\d*");
    for (String isnString : cmdsList) {
      if (isnString != null) {
        if (isnString.contains(" ")) {
          Matcher regExMatcher = regExPattern.matcher(isnString);
          while (regExMatcher.find()) {
            cmdsParsed.add(regExMatcher.group());
          }
        } else {
          cmdsParsed.add(isnString);
        }
      }
    }
    return cmdsParsed;
  }

  /**
   * Returns the ErrorHandler object for the command translator. Assumes that the Translator has a
   * valid ErrorHandler object.
   *
   * @return the object's ErrorHandler
   */
  public ErrorHandler getMyErrorHandler() {
    return myErrorHandler;
  }

  protected ResourceBundle getMyErrorsBundle() {
    return myErrorsBundle;
  }

}

