package oolala.controller.translator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import oolala.controller.translator.CommandTranslator;
/**
 * Implements the CommandTranslator for the specific Darwin application. See comments on
 * superclass CommandTranslator. This particular class could fail if it is not passed Darwin
 * commands, or is not used alongside the DarwinManager and DarwinAnimal. It would be used in a
 * Darwin simulation as a way to translate commands passed in.
 */
public class DarwinTranslator extends CommandTranslator {



  private Map<String, String> myAlphabet;
  private List<String> myInstructions;

  public DarwinTranslator() {
    super();
    myAlphabet = new HashMap<>(
        Map.of("move", "fd", "left", "lt", "right", "rt"));
    myInstructions = new ArrayList<>(
        List.of("fd", "lt", "rt", "move", "left", "right", "go", "ifenemy", "ifrandom", "ifempty",
            "ifsame", "ifwall", "infect"));
  }

  /**
   * Translate the lines of darwin instructions into logo instructions by replacing darwin commands
   * with the corresponding logo instructions, if found. Overrides method in superclass to
   * translate the Darwin translation. Additionally, checks to make sure all instructions
   * passed are Darwin instructions.
   *
   * @param cmdsList List of darwin instructions passed
   * @return List of logo instructions from darwin input
   */
  @Override
  public List<String> translateLinesIntoLogoIsns(List<String> cmdsList) {
    List<String> translatedList = new ArrayList<>(cmdsList);
    for (int i = 0; i < cmdsList.size(); i++) {
      for (String key : myAlphabet.keySet()) {
        String currentItem = cmdsList.get(i);
        if (currentItem.contains(key)) {
          currentItem = currentItem.replaceAll(key, myAlphabet.get(key));
          translatedList.set(i, currentItem);
        }
      }
    }
    checkInstructions(translatedList);
    return translatedList;
  }

  private void checkInstructions(List<String> translatedList) {
    for (String s : translatedList) {
      if (!myInstructions.contains(s.split(" ")[0]) && !s.equals("")) {
        getMyErrorHandler().setErrorFound(getMyErrorsBundle().getString("DarwinInvalid"));
        //translatedList.remove(s);
      }
    }
  }
}