package oolala.controller.translator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Extends the CommandTranslator Class to specifically translate LSystem rule, start, and set
 * instructions into logo instructions that can be made into Command objects. Again, has many of
 * the same assumptions and dependencies as the CommandTranslator superclass. Must be used with a
 * Command Generator and a Controller to pass the translated instructions to, and to receive
 * untranslated instructions. Must be given correct LSystem instructions, and the correct number
 * of instructions to work.
 */
public class LSystemTranslator extends CommandTranslator {



  private int myNumLevels;
  private Map<String, String> myAlphabet;
  private Map<String, String> myRules;
  private String myStart;
  private String myLength;
  private String myAngle;


  /**
   * Constructor for LSystem, sets the translator up with the given length, angle, and number of
   * levels to do for the game. Must be passed valid parameters for these values, or the translator
   * will not be able to generate the pattern or specified commands.
   *
   * @param length    length of each movement in the drawing process
   * @param angle     angle to turn by for each turn command
   * @param numLevels number of levels to expand out the pattern given
   */
  public LSystemTranslator(String length, String angle, String numLevels) {
    super();
    myLength = length;
    myAngle = angle;
    myNumLevels = Integer.parseInt(numLevels);
    myAlphabet = new HashMap<>(
            Map.of("f", String.format("pd fd %s", length), "g", String.format("pu fd %s", length),
                    "a", String.format("pu bk %s", length), "b", String.format("pd bk %s", length), "+",
                    String.format("rt %s", angle), "-", String.format("lt %s", angle), "x", "stamp"));
    myRules = new HashMap<>();
  }

  /**
   * Translate lines of LSystem instructions into logo instructions. Overrides method in superclass
   * to specifically translate the provided lines into LSystem instructions. This is a somewhat
   * complex process, as the instructions must be parsed, the correct pattern must be generated, and
   * then the pattern must be converted to logo instructions.
   *
   * @param cmdsList input list of LSystem instructions given
   * @return list of logo instructions that has been translated
   */
  @Override
  public List<String> translateLinesIntoLogoIsns(List<String> cmdsList) {
    List<String> levelExpressions = new ArrayList<>();
    List<String> logosExpressions = new ArrayList<>();

    readAndStoreInstructions(cmdsList);
    expandForEachLevel(levelExpressions);
    convertFromPatternToLogoIsns(levelExpressions, logosExpressions);
    return logosExpressions;
  }

  private void convertFromPatternToLogoIsns(List<String> levelExp, List<String> logosExp) {
    logosExp.addAll(List.of("pu", "ht"));
    for (int i = 0; i < myNumLevels; i++) {
      logosExp.addAll(List.of("home", String.format("fd %s", (i * 70)), "rt 90"));
      try {
      for (String symbol : levelExp.get(i).split("")) {

          String logoEquivalent = myAlphabet.get(symbol);
          if (logoEquivalent == null && i != 0) {
            throw new NullPointerException();
          }
          if (logoEquivalent != null){
            logosExp.add(logoEquivalent);
          }
        }
        logosExp.add("pu");
      }
      catch (NullPointerException e) {
          getMyErrorHandler().setErrorFound(
                  getMyErrorsBundle().getString("IllegalLSysInstruction"));
          logosExp.clear();
        }
    }
  }

  private void expandForEachLevel(List<String> eachLevel) {
    String myCurrent = myStart;
    eachLevel.add(myCurrent);
    for (int level = 1; level < myNumLevels; level++) {
      List<String> myCurrentSplitLetters = Arrays.asList(myCurrent.split(""));
      for (int idx = 0; idx < myCurrentSplitLetters.size(); idx++) {
        String currentLetter = myCurrentSplitLetters.get(idx);
        myCurrentSplitLetters.set(idx, myRules.getOrDefault(currentLetter, currentLetter));
      }
      myCurrent = String.join("", myCurrentSplitLetters);
      eachLevel.add(myCurrent);
    }
  }

  private void readAndStoreInstructions(List<String> cmdsList) {
    List<String> splitIsnList = new ArrayList<>();
    splitIntoIndividualIsns(cmdsList, splitIsnList);
    setParamsFromIsns(splitIsnList);
    if (myStart == null || myRules.isEmpty()) {
      getMyErrorHandler().setErrorFound(getMyErrorsBundle().getString("NotEnoughLSystemCmds"));
      splitIsnList.clear();
    }
  }

  private void splitIntoIndividualIsns(List<String> cmdsList, List<String> splitIsnList) {
    Pattern regExPattern = Pattern.compile("(start\\s+\\S|rule\\s+\\S\\s+\\S+|set\\s+.+)");
    for (String line : cmdsList) {
      line = line.toLowerCase(Locale.ROOT);
      Matcher regExMatcher = regExPattern.matcher(line);
      while (regExMatcher.find()) {
        splitIsnList.add(regExMatcher.group());
      }
    }
  }

  private void setParamsFromIsns(List<String> splitIsnList) {
    for (String line : splitIsnList) {
      try {
        String[] splitLine = line.split("\\s+");
        String cmdType = splitLine[0].toLowerCase();
        String symbol = splitLine[1];
        switch (cmdType) {
          case "start":
            myStart = symbol;
            break;
          case "rule":
            myRules.put(symbol, splitLine[2]);
            break;
          case "set":
            parseSetInput(line, symbol);
            break;
          default:
            throw new IllegalArgumentException();
        }
      } catch (IllegalArgumentException e) {
        getMyErrorHandler().setErrorFound(getMyErrorsBundle().getString("InvalidInstruction"));
      }
    }
  }

  private void parseSetInput(String line, String symbol) {
    String cmd = line.split("\\s+\"")[1];
    cmd = cmd.replaceAll("\"", "");
    cmd = cmd.replaceAll("length", myLength);
    cmd = cmd.replaceAll("angle", myAngle);
    myAlphabet.put(symbol, cmd);
  }
}
