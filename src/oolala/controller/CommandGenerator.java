package oolala.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.ResourceBundle;
import oolala.commands.Command;
/**
 * Generates Command objects given a list of logo/basic instructions that correspond to specific
 * Command classes. Creates these Commands and sets the appropriate values for each. Assume that
 * the user inputs valid instructions that are correctly formatted and match up to Command
 * classes. If bad instructions are inputted, the CommandGenerator will handle the error, but will
 * not be able to complete the command generation successfully. The CommandGenerator is dependent
 * on the CommandTranslator class, as it has to provide correctly translated instructions, as well
 * as the Controller class, to do meaningful things with the Commands after they are generated. An
 * example of using this would be inputting a list of formatted logo instructions, calling create
 * commands, and then getting back a Queue of Command objects.
 */
public class CommandGenerator {


  public static final String INSTRUCTION_SET_RESOURCE_BUNDLE = "oolala.controller.resources.LogoInstructions";
  private final ResourceBundle myInstructionSet;
  private ResourceBundle myErrorsBundle;
  private final ErrorHandler myErrorHandler;

  /**
   * Constructor for CommandGenerator. Sets the resource bundle that contains the translation from
   * logo instructions to Command classes, and creates a new error handler. Assumes that a valid
   * resource bundle is provided that contains all needed instructions.
   */
  public CommandGenerator() {
    myInstructionSet = ResourceBundle.getBundle(INSTRUCTION_SET_RESOURCE_BUNDLE);
    myErrorHandler = new ErrorHandler();
  }

  protected void setMyErrorsBundle(ResourceBundle errors) {
    myErrorsBundle = errors;
  }

  protected Queue<Command> createCommands(List<String> instructions) {
    Queue<Command> commandsQueue = new LinkedList<>();
    for (String instruction : instructions) {
      if (!instruction.equals("")) {
        try {
          String[] splitInstruction = instruction.split(" ");
          Command newCommand = generateCommandObject(splitInstruction[0]);
          addValueToCommand(splitInstruction, newCommand);
          if (newCommand != null) {
            commandsQueue.add(newCommand);
          }
        } catch (Exception e) {
          myErrorHandler.setErrorFound(myErrorsBundle.getString("InvalidOriginalParams"));
        }
      }
    }
    return commandsQueue;
  }

  private void addValueToCommand(String[] splitInstruction, Command newCommand) {
    if (splitInstruction.length > 1) {
      try {
        double paramValue = Double.parseDouble(splitInstruction[1]);
        newCommand.setMyValue(paramValue);
      } catch (NumberFormatException e) {
        myErrorHandler.setErrorFound(myErrorsBundle.getString("InvalidOriginalParams"));
      }
    }
  }

  private Command generateCommandObject(String currentIsn) {
    try {
      return (Command) Class.forName(
              String.format("oolala.commands.%s", myInstructionSet.getString(currentIsn)))
          .getConstructor().newInstance();
    } catch (Exception e) {
      myErrorHandler.setErrorFound(
          String.format("%s %s", myErrorsBundle.getString("IsnHelpMessage"), currentIsn));
    }
    return null;
  }

  /**
   * Returns the ErrorHandler, which contains error information collected during execution, for the
   * command generator
   *
   * @return the ErrorHandler for the CommandGenerator
   */
  public ErrorHandler getMyErrorHandler() {
    return myErrorHandler;
  }
}
