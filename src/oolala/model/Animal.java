package oolala.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.commands.Command;

/**
 * The Animal class is the main object that is acting in each of the oolala games. Animal is the
 * object that does the actions specified by commands. This abstract class is the base for both
 * the DarwinAnimal and DrawingAnimal classes, and contains all the common functionality that an
 * Animal has between all three games. This class may fail if not used with an Animal Manager, and
 * if it does not contain an AnimalData object. It also must execute Command objects, so it is
 * dependent on the commands package and the Command object. This abstract class can be extended
 * to create different types of animals with increased functionality, as has been done with
 * DrawingAnimal and DarwinAnimal.
 */
public abstract class Animal {



  private final AnimalData myAnimalData;
  private static final double DEGREES_CONSTANT = 180;
  private static final double MY_AREA_SIZE = 400;
  private static final double MY_WALL_OFFSET = 5;
  private static final double MY_TITLE_OFFSET = 40;
  private static final double MY_STARTING_ANGLE = 90;
  private static final double MY_WALL_TOLERANCE = 5;

  private Collection<Command> commandsToExecute;

  /**
   * Constructor for Animal object. Since it is abstract, this is never called directly. Assume that
   * user implements classes that extend Animal.
   *
   * @param id id for new Animal to be created.
   */
  protected Animal(int id) {
    myAnimalData = new AnimalData(id);
    commandsToExecute = new LinkedList<>();
  }

  /**
   * Adds all commands in a queue to the Animal's Collection of Commands
   *
   * @param toAdd queue of Command Objects to give to an Animal
   */
  public void addCommands(Queue<Command> toAdd) {
    commandsToExecute.addAll(toAdd);
  }

  /**
   * Gets the next command that an Animal needs to execute
   *
   * @return the next command for the animal to execute
   */
  public abstract Command getNextCommand();

  public void setCommandsToExecute(Collection<Command> newCommands) {
    commandsToExecute = newCommands;
  }

  /**
   * Gets the Collection of an Animals current commands
   *
   * @return an Animal's current commands
   */
  public Collection<Command> getCommandsToExecute() {
    return commandsToExecute;
  }

  public void clearCommands() {
    commandsToExecute.clear();
  }

  /**
   * Gets an Animals current AnimalData (all the information about it)
   *
   * @return the current AnimalData object for the animal
   */
  public AnimalData getMyAnimalData() {
    return myAnimalData;
  }

  public boolean isEmpty() {
    return commandsToExecute.isEmpty();
  }

  /**
   * Resets the position of an animal to its set home position and starting angle. May fail if an
   * Animal's home data is cleared out or invalid.
   */
  public void resetPosition() {
    List<Double> homeData = myAnimalData.getMyHome();
    myAnimalData.setMyX(homeData.get(0));
    myAnimalData.setMyY(homeData.get(1));
    myAnimalData.setAngle(MY_STARTING_ANGLE);
  }

  /**
   * Moves Animal a given distance in the correct direction based on its current angle and the
   * distance provided if it will not run into a wall by doing so
   *
   * @param distance number of pixels to move
   */
  public void move(double distance) {
    double newX = myAnimalData.getMyX() + distance * Math.cos(
        myAnimalData.getAngle() * Math.PI / DEGREES_CONSTANT);
    double newY = myAnimalData.getMyY() + distance * Math.sin(
        myAnimalData.getAngle() * Math.PI / DEGREES_CONSTANT);
    if (!wallNearMe(newX, newY)) {
      myAnimalData.setMyX(newX);
      myAnimalData.setMyY(newY);
    }
  }

  /**
   * Rotates Animal by a given angle
   *
   * @param angle degrees to rotate by
   */
  public void rotate(double angle) {
    myAnimalData.setAngle(myAnimalData.getAngle() + angle);
  }

  /**
   * Checks if there is a wall near the specified location
   *
   * @param x x coordinate to check
   * @param y y coordinate to check
   * @return if there is a wall by the specified location
   */
  public boolean wallNearMe(double x, double y) {
    return (wallDetected(x, MY_WALL_OFFSET) || wallDetected(y, MY_TITLE_OFFSET));
  }

  private boolean wallDetected(double coordinate, double offset) {
    return (MY_AREA_SIZE - coordinate <= MY_WALL_TOLERANCE
        || coordinate <= MY_WALL_TOLERANCE+offset);
  }

  /**
   * Overrides .equals to compare Animals based on ID instead of memory address
   *
   * @param obj Animal to compare to
   * @return whether the two Animals have the same ID (therefore are the same)
   */
  @Override
  public boolean equals(Object obj) {
    if (this.getClass() != obj.getClass()){
      return false;
    }
    Animal otherAnimal = (Animal) obj;
    return myAnimalData.getMyID() == otherAnimal.getMyAnimalData().getMyID();
  }
}

