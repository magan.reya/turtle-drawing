package oolala.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.commands.Command;

/**
 * DarwinAnimal has the functionality to be used in a darwin simulation and extends the abstract
 * Animal class. Implements methods specific to animal behavior for the darwin simulation. Assumes
 * that it is used with a DarwinManager and will be interacting with other DarwinAnimals. So, it
 * is dependent on these classes. Also assumes that it has an AnimalData object. May fail if not
 * used with other Darwin implementations. Can be used as an Animal object for a darwin
 * simulation. When running a darwin simulation, these DarwinAnimals will be the objects
 * participating in the simulation.
 */
public class DarwinAnimal extends Animal {



  private int cmdCurrentIndex = 1;

  /**
   * DarwinAnimal constructor, creates new DarwinAnimal
   *
   * @param id   specify id for new animal being created
   * @param type specify type of animal to be created
   */
  public DarwinAnimal(int id, String type) {
    super(id);
    setCommandsToExecute(new ArrayList<>());
    getMyAnimalData().setMyType(type);
  }

  /**
   * Gets the next command for a DarwinAnimal to execute by moving to the next instruction in the
   * list of commands. Assumes that the DarwinAnimal has commands that have been loaded.
   *
   * @return the next command to be executed
   */
  @Override
  public Command getNextCommand() {
    List<Command> curCommands = new ArrayList<>(getCommandsToExecute());
    checkThatCommandIndexisInBounds(curCommands.size());
    Command nextCmd = curCommands.get(cmdCurrentIndex - 1);
    cmdCurrentIndex++;
    checkThatCommandIndexisInBounds(curCommands.size());
    return nextCmd;
  }

  private void checkThatCommandIndexisInBounds(int cmdsSize) {
    if (cmdCurrentIndex > cmdsSize || cmdCurrentIndex <1){
      cmdCurrentIndex = 1;
    }
  }

  /**
   * Sets a new index continue execution from in DarwinAnimals list of commands
   * @param newIdx
   */
  public void setCmdCurrentIndex(int newIdx) {
    cmdCurrentIndex = newIdx;
  }


  public int getCmdCurrentIndex() {
    return cmdCurrentIndex;
  }
  /**
   * changes other animals species to this one's
   * @param victimAnimals
   */
  public void infect(List<Animal> victimAnimals) {
    for (Animal toInfect : victimAnimals) {
      DarwinAnimal victim = (DarwinAnimal) toInfect;
      victim.getMyAnimalData().setMyType(getMyAnimalData().getMyType());
      victim.clearCommands();
      victim.addCommands(convertCommandsToQueue());
      victim.setCmdCurrentIndex(1);

    }
  }

  private Queue<Command> convertCommandsToQueue() {
    Queue<Command> curCommands = new LinkedList<>();
    for (Command c : this.getCommandsToExecute()) {
      curCommands.add(c);

    }
    return curCommands;
  }
}


