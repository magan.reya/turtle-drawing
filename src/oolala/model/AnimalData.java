package oolala.model;

import java.util.List;

/**
 * AnimalData is used to store all the data related to an Animal object. It is assumed that this
 * class will be used alongside the Animal class, as all Animals are composed of AnimalData.
 * Therefore, it is dependent on an active Animal object to use the stored values. For example,
 * the DarwinAnimal class holds AnimalData. This class allows us to pass information about an
 * Animal object without passing the object itself. All setter methods in this class are
 * protected, so it is not editable by classes outside the model package.
 */
public class AnimalData {



  private double myX;
  private double myY;
  private double myHomeX;
  private double myHomeY;
  private double myAngle;
  private final int myID;
  private boolean showImage = true;
  private boolean myStamp = false;
  private boolean myPen = false;
  private String myType;
  private static final double DEFAULT_HOME_POSITION = 200;
  private static final double DEFAULT_ANGLE = 90;

  public AnimalData(int id) {
    myID = id;
    myX = DEFAULT_HOME_POSITION;
    myY = DEFAULT_HOME_POSITION;
    setMyHome(DEFAULT_HOME_POSITION, DEFAULT_HOME_POSITION);
    myAngle = DEFAULT_ANGLE;
    myType = "turtle";
  }

  protected void setMyHome(double x, double y) {
    myHomeX = x;
    myHomeY = y;
  }

  /**
   * used to get an animal's currently stored values for its home position. must have valid home
   * coordinates stored to function as expected.
   *
   * @return animal's home coordinates
   */
  public List<Double> getMyHome() {
    return List.of(myHomeX, myHomeY);
  }

  /**
   * used to get an animal's id number. must have valid id stored to work.
   *
   * @return animal's identification number
   */
  public int getMyID() {
    return myID;
  }

  protected void setMyX(double xVal) {
    myX = xVal;
  }

  /**
   * used to get an animal's current x coordinate. must have valid x coordinate stored to work.
   *
   * @return animal's x coordinate in the game
   */
  public double getMyX() {
    return myX;
  }

  protected void setMyY(double yVal) {
    myY = yVal;
  }

  /**
   * used to get an animal's current y coordinate. must have valid y coordinate stored to work.
   *
   * @return animal's y coordinate in the game
   */
  public double getMyY() {
    return myY;
  }

  protected void setAngle(double newAngle) {
    myAngle = newAngle;
  }

  /**
   * used to get an animal's current angle of orientation. must have valid angle stored to work.
   *
   * @return animal's current angle
   */
  public double getAngle() {
    return myAngle;
  }

  protected void setShow(Boolean isSeen) {
    showImage = isSeen;
  }

  /**
   * used to get an animal's current visibility status.
   *
   * @return whether an animal is currently visible
   */
  public boolean getShow() {
    return showImage;
  }

  protected void setStamp() {
    myStamp = true;
  }

  /**
   * used to get an animal's current stamp status and then reset the stamp status to false.
   *
   * @return animal's current stamp status
   */
  public boolean getStamp() {
    boolean stampHolder = myStamp; //hack to ensure that stamp is set to false after being queried
    myStamp = false;
    return stampHolder;
  }

  protected void setPenStatus(boolean penStatus) {
    myPen = penStatus;
  }

  /**
   * used to get an animal's current pen status.
   *
   * @return animal's current pen status
   */
  public boolean getPenStatus() {
    return myPen;
  }

  /**
   * used to get an animal's type. assumes that animal was set with correct type originally.
   *
   * @return animal's type
   */
  public String getMyType() {
    return myType;
  }

  protected void setMyType(String newType) {
    myType = newType;
  }
}

