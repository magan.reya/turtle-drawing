package oolala.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import oolala.commands.Command;
/**
 * DarwinManager extends AnimalManager and implements all the abstract methods to support the
 * functionality of the Darwin game. It is useful in organizing all the DarwinAnimals in a game
 * and implementing the specific functionality needed for the simulation. Most importantly, it
 * allows us to control the interactions of the many animals in the Darwin simulation. We assumed
 * that the DarwinManager will have DarwinAnimals, all of which have valid commands. This class is
 * dependent on both the DarwinAnimal and the model classes to be functioning correctly. An
 * example of using this would be creating a new DarwinManager, and adding  DarwinAnimals to it
 * via createAnimalsForGame.
 */
public class DarwinManager extends AnimalManager {



  private List<String> mySpecies;
  private String myCurrentSpecies;
  private static final int MY_ANIMAL_LOCATION_OFFSET = 50;
  private static final int MY_ANIMAL_STARTING_AREA_SIZE = 300;
  private static final int NUM_ANIMAL_PER_SPECIES = 10;
  private double myRadius = 40;
  private Random myRandom;

  /**
   * Constructor for DarwinAnimal, sets mySpecies to be an empty ArrayList and myCurrentSpecies to
   * be a turtle upon initialization
   */
  public DarwinManager() {
    super();
    mySpecies = new ArrayList<>();
    myCurrentSpecies = "turtle";
    myRandom = new Random();
  }

  /**
   * Creates animals for the DarwinGame, in this case, it is 10 animals of each species, and the
   * animals are inserted at random locations. This assumes that the species inputted is a unique
   * species that hasn't been inputted before.
   *
   * @param species
   */
  @Override
  public void createAnimalsForGame(String species) {
    mySpecies.add(species);
    for (int i = 0; i < NUM_ANIMAL_PER_SPECIES; i++) {
      DarwinAnimal newAnimal = new DarwinAnimal(getAnimals().size(), species);
      while (!getOthersAround(newAnimal).isEmpty()) {
        newAnimal.getMyAnimalData()
            .setMyX(MY_ANIMAL_LOCATION_OFFSET + myRandom.nextInt(MY_ANIMAL_STARTING_AREA_SIZE));
        newAnimal.getMyAnimalData()
            .setMyY(MY_ANIMAL_LOCATION_OFFSET + myRandom.nextInt(MY_ANIMAL_STARTING_AREA_SIZE));
      }
      getAnimals().add(newAnimal);
    }
    myCurrentSpecies = species;
  }

  /**
   * Adds a given Queue of Commands to all animals of the most recently initialized species, which
   * is set as myCurrentSpecies. Assumes that myCurrentSpecies contains a valid species that has
   * been previously added.
   *
   * @param toAdd queue of commands to be added to each animal of the given species
   */
  @Override
  public void addCommands(Queue<Command> toAdd) {
    addCommandsToSpecifiedAnimals(toAdd, getAllAnimalsOfAParticularSpecies(myCurrentSpecies));
  }

  private String getRandomSpecies() {
    return mySpecies.get(myRandom.nextInt(mySpecies.size()));
  }

  /**
   * Executes the next command for each animal in a given species (species is selected randomly from
   * all species currently in game). Assumes that there is at least one species in the game.
   *
   * @return
   */
  @Override
  public void executeCommand() {
    String speciesToActOn = getRandomSpecies();
    List<Animal> animalsToActOn = getAllAnimalsOfAParticularSpecies(speciesToActOn);
    executeCommandsOnSpecifiedAnimals(animalsToActOn);
  }

  /**
   * Changes the radius that the the DarwinManager uses to detect if DarwinAnimals are close
   * together or not. A valid value for the radius is assumed to have been passed.
   *
   * @param radius the new radius to set
   */
  public void radiusChange(double radius) {
    myRadius = radius;
  }

  private List<Animal> getAllAnimalsOfAParticularSpecies(String speciesToActOn) {
    List<Animal> animalsToActOn = new ArrayList<>();

    for (Animal curAnimal : getAnimals()) {
      if (curAnimal.getMyAnimalData().getMyType().equals(speciesToActOn)) {
        animalsToActOn.add(curAnimal);
      }
    }
    return animalsToActOn;
  }

  /**
   * Returns animals of different species in startingAnimal's vicinity, determined by the radius
   * that we set earlier in the game.
   *
   * @param startingAnimal the animal we want to detect others around
   * @return the list of animals "close" to the animal passed
   */
  public List<Animal> getOthersAround(Animal startingAnimal) {
    List<Animal> animalsNearMe = new ArrayList<>();
    double startingX = startingAnimal.getMyAnimalData().getMyX();
    double startingY = startingAnimal.getMyAnimalData().getMyY();
    for (Animal curAnimal : getAnimals()) {
      double distance = calculateDistance(startingX, startingY,
          curAnimal.getMyAnimalData().getMyX(),
          curAnimal.getMyAnimalData().getMyY());
      if (distance < myRadius && curAnimal != startingAnimal) {
        animalsNearMe.add(curAnimal);
      }
    }
    return animalsNearMe;
  }


  private double calculateDistance(double x1, double y1, double x2, double y2) {
    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
  }

  protected String getMyCurrentSpecies() {
    return myCurrentSpecies;
  }

  /**
   * Clears out all the DarwinAnimals that the DarwinManager holds when starting a new game.
   */
  @Override
  public void clearOutForNewGame() {
    getAnimals().clear();

  }
}
