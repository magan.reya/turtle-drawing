package oolala.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import oolala.commands.Command;

/**
 * The GameModel class is the primary engine of the Game. It triggers the step of command
 * execution, contains the AnimalManager, and works to transfer data from the controller to the
 * Animals and AnimalManager. We assumed that this can function somewhat independently, as it does
 * not have access to the controller. Despite this, we do assume that there is a connection with
 * the controller and view to be able to display data from what is happening in the
 * simulation/drawing game graphically. We assume that all the components that make up the model,
 * including the Animal and the Animal Manager are functioning correctly. It is dependent on these
 * classes, as well as on the Commands class. An example is the model being created to drive the
 * backend of a simulation, and being connected to the controller/view to display what is
 * happening in the model.
 */
public class GameModel {



  private AnimalManager myAnimalManager;
  private Map<Integer, AnimalData> dataToActOn;

  public GameModel() {
    myAnimalManager = new DarwinManager();
    dataToActOn = new HashMap<>();
  }

  /**
   * Executes step in the game, which includes executing a command and then subsequently updating
   * the map of the data for all animals if there are instructions available. Assumes that Animal
   * Manager has animals.
   */
  public void executeStep() {
    if (!outOfInstructions()) {
      myAnimalManager.executeCommand();
      updateDataMap();
    }
  }

  /**
   * Updates the map of id : AnimalData that the model holds to keep track of most current animal
   * information. Assumes that AnimalManager has animals.
   */
  public void updateDataMap() {
    for (Animal currentAnimal : myAnimalManager.getAnimals()) {
      AnimalData currentAnimalData = currentAnimal.getMyAnimalData();
      updateDataMap(currentAnimalData.getMyID(), currentAnimalData);
    }
  }

  /**
   * Creates Animals of a particular species for the game and adds them to the AnimalManager, exact
   * implementation will depend on which AnimalManager is being used.
   *
   * @param species the species that the user wants to set up for a particular game
   */
  public void setupAnimals(String species) {
    myAnimalManager.createAnimalsForGame(species);
  }

  /**
   * Adds a queue of generated commands to Animals in the game. How the commands are added and to
   * which animals will depend on which AnimalManager is used. Assumes that the user is adding a
   * valid list of Commands.
   *
   * @param toAdd list of Commands given to AnimalManager to distribute
   */
  public void addCommands(Queue<Command> toAdd) {
    myAnimalManager.addCommands(toAdd);
  }

  /**
   * Returns all the AnimalData for the animals in the game. Assumes that there is data available
   * and the map is not empty.
   *
   * @return all AnimalData for animals in game
   */
  public Collection<AnimalData> getAllData() {
    return dataToActOn.values();
  }

  protected boolean outOfInstructions() {
    return (myAnimalManager.getAnimals().isEmpty() || myAnimalManager.getAnimals().get(0)
        .isEmpty());
  }

  private void updateDataMap(int identificationNum, AnimalData currentAnimalData) {
    dataToActOn.put(identificationNum, currentAnimalData);
  }

  /**
   * Gets the current AnimalManager. Assumes that the AnimalManager is not null.
   *
   * @return the current AnimalManager
   */
  public AnimalManager getMyAnimalManager() {
    return myAnimalManager;
  }

  /**
   * Sets the current AnimalManager to be a specified type of Animal Manager. Assumes that a valid
   * AnimalManager is passed that matches with the other subclasses, i.e. DarwinManager and
   * DarwinTranslator.
   *
   * @param myAnimalManager the animal manager to use for the game
   */
  public void setMyAnimalManager(AnimalManager myAnimalManager) {
    this.myAnimalManager = myAnimalManager;
  }

  /**
   * Sets radius the AnimalManager will to determine animal proximity
   *
   * @param radius the value passed to set for the radius
   */
  public void setNewRadius(double radius) {
    DarwinManager darwinMgr = (DarwinManager) myAnimalManager;
    darwinMgr.radiusChange(radius);
  }

  /**
   * In the event of new game call, clears out all the old data from the model and
   * tells the AnimalManager to clear out all the Animals to start a new game
   */
  public void clearOutModel() {
    dataToActOn.clear();
    myAnimalManager.clearOutForNewGame();
  }

}
