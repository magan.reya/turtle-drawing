package oolala.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import oolala.commands.Command;
/**
 * This abstract class manages all the animals in the game. It is useful because it provides a way
 * to keep many animals in the game organized and distribute Commands. It stores all the animals,
 * and is in charge of determining their interactions with each other. We assumed that this class
 * is used in conjunction with Animals and will have Animals that it holds. This class may fail if
 * there are no Animals that have been initialized in it. Additionally, this class is dependent on
 * other classes in the Model package, since to distribute commands, it needs to receive them from
 * the CommandGenerator. Since this class is abstract, it must be extended, so the user can create
 * a class that extends this and implements the specific functionality for managing animals in
 * their game. An example of using this would be creating a DarwinManager, that extends this class
 * and manages DarwinAnimals. This class would then control how these animals interact and receive
 * commands.
 */
public abstract class AnimalManager {



  private List<Animal> myAnimals;

  /**
   * Constructor for AnimalManager(), creates a new AnimalManager with an empty ArrayList to hold
   * Animals
   */
  protected AnimalManager() {
    myAnimals = new ArrayList<>();
  }

  /**
   * Creates animals for a game given a particular species. The number and location of the species
   * is dependent on the implementation of the method. Assumes that the user inputs a unique species.
   *
   * @param species the type of animal you would like to create
   */
  protected abstract void createAnimalsForGame(String species);

   /**
   * Returns a list of all the animals in the game. Assumes that the list contains valid animals.
   *
   * @return myAnimals the list of Animals in the game
   */
  public List<Animal> getAnimals() {
    return myAnimals;
  }

  /**
   * Adds command to animals in the game. The specific animals that the commands are added to is
   * determined by the implementation of the method. Assumes that a Queue of valid commands is
   * passed.
   *
   * @param toAdd the Queue of commands that we want to add to animals in the game
   */
  protected abstract void addCommands(Queue<Command> toAdd);

  protected void addCommandsToSpecifiedAnimals(Queue<Command> toAdd, List<Animal> animalsToActOn) {
    int numAnimals = animalsToActOn.size();
    for (int index = 0; index < numAnimals; index++) {
      Animal currentAnimal = animalsToActOn.get(index);
      currentAnimal.addCommands(toAdd);
    }
  }

  /**
   * Tells animals in the game to execute their next command. Assumes that animals have been given
   * commands and that they all have a valid next command.
   *
   * @return List of animals that have executed commands or been changed during the execution of
   * commands.
   */
  protected abstract void executeCommand();

  protected List<Animal> executeCommandsOnSpecifiedAnimals(List<Animal> animalsToActOn) {
    int numAnimals = animalsToActOn.size();
    for (int index = 0; index < numAnimals; index++) {
      Animal currentAnimal = animalsToActOn.get(index);
      boolean isnCompleted = false;
      while (!isnCompleted) {
        isnCompleted = currentAnimal.getNextCommand().executeInstruction(currentAnimal, this);
      }
    }
    return myAnimals;
  }

  /**
   * Removes all animals from the back end at the start of a new game. Assumes that there are
   * animals to clear out and that the user has already played at least one game.
   */
  protected abstract void clearOutForNewGame();

  /**
   * Sets all animals to have a new home position. Assumes valid x and y coordinates passed and a
   * functioning setMyHome function
   *
   * @param x the input x coordinate for the new home
   * @param y the input y coordinate for the new home
   */
  public void setNewHomeForAnimals(double x, double y) {
    for (Animal curAnimal : myAnimals) {
      curAnimal.getMyAnimalData().setMyHome(x, y);
    }
  }

}
