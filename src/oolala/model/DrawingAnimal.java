package oolala.model;

import java.util.LinkedList;
import java.util.Queue;
import oolala.commands.Command;

public class DrawingAnimal extends Animal {

  /**
   * DrawingAnimal has the functionality to be used in drawing applications and extends the abstract
   * Animal class. It has behaviors related to drawing and creating designs. Assumes that it is used
   * with the DrawingManager class and contains AnimalData. Dependent on working with these other
   * classes to function. If it is not used with other implementations for drawing applications,
   * would see a failure. Can create a DrawingAnimal and drawing commands, which would show up on
   * the user interface when executed in the application.
   *
   * @param id input the id for a new DrawingAnimal to be created
   */

  public DrawingAnimal(int id) {
    super(id);
    setCommandsToExecute(new LinkedList<>());
    getMyAnimalData().setMyType("turtle");
    getMyAnimalData().setPenStatus(true);
  }

  /**
   * Calls remove on the DrawingAnimal's queue. This gets the next command that the
   * DrawingAnimal will execute. Assumes that a valid command is present.
   *
   * @return command for DrawingAnimal to execute next
   */
  @Override
  public Command getNextCommand() {
    Queue<Command> curCommands = new LinkedList<>(getCommandsToExecute());
    Command currentCommand = curCommands.remove();
    setCommandsToExecute(curCommands);
    return currentCommand;
  }

  /**
   * Creates another DrawingAnimal with a given ID from the current one
   * @param id id for the new DrawingAnimal
   * @return new Drawing Animal with given id
   */
  public DrawingAnimal replicate(int id) {
    return new DrawingAnimal(id);
  }

  /**
   * Sets the DrawingAnimal's visibility to on
   */
  public void showAnimal() {
    getMyAnimalData().setShow(true);
  }

  /**
   * Sets the DrawingAnimal's visibility to off
   */
  public void hideAnimal() {
    getMyAnimalData().setShow(false);
  }

  /**
   * Sets the DrawingAnimal to stamp the next time stamp is called
   */
  public void stampAnimal() {
    getMyAnimalData().setStamp();
  }

  /**
   * Sets the DrawingAnimal's pen to be up and not mark
   */
  public void penUp() {
    getMyAnimalData().setPenStatus(false);
  }

  /**
   * Sets the DrawingAnimal's pen to be down and mark
   */
  public void penDown() {
    getMyAnimalData().setPenStatus(true);
  }
}
