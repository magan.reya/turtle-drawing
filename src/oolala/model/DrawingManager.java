package oolala.model;

import java.util.List;
import java.util.Queue;
import oolala.commands.Command;
/**
 * DrawingManager extends AnimalManager and implements all the abstract methods to support the
 * functionality of any Drawing game. It is useful in organizing all of the DrawingAnimals in a
 * game and implementing the specific functionality needed for the simulation. Most importantly,
 * it allows us to control the interactions of animals in the Drawing games, particularly related
 * to the "TELL" command and working with multiple animals. We assumed that the DrawingManager
 * will have at least 1 DrawingAnimal, all of which will have valid commands. This class is
 * dependent on both the DrawingAnimal and the model classes to be functioning correctly. It is
 * also very dpendent on the Commands class, as this controls the functionality that the
 * DrawingManager is managing. An example of using this would be creating a new DrawingManager,
 * and using a method to a new turtle to it via createAnimalsForGame.
 */
public class DrawingManager extends AnimalManager {



  /**
   * Constructor for DrawingManager, creates a new turtle in the game
   */
  public DrawingManager() {
    super();
    createAnimalsForGame("turtle");
  }

  /**
   * Creates animals for the game, which for the drawing game, is one turtle animal to begin.
   *
   * @param species the type of animal you would like to create. for drawing game, this will always
   *                be "turtle"
   */
  @Override
  public void createAnimalsForGame(String species) {
    DrawingAnimal startingAnimal = new DrawingAnimal(0);
    startingAnimal.getMyAnimalData().setMyType(species);
    getAnimals().add(startingAnimal);
  }

  /**
   * Adds commands for a specified species, which in the drawing game, is all the animals in the
   * game.
   *
   * @param toAdd
   */
  @Override
  public void addCommands(Queue<Command> toAdd) {
    addCommandsToSpecifiedAnimals(toAdd, getAnimals());
  }

  /**
   * Executes command on specified animals, in this case, it is all animals in the drawing game.
   *
   * @return the list of animals that executed commands
   */
  @Override
  public void executeCommand() {
    executeCommandsOnSpecifiedAnimals(getAnimals());
  }

  /**
   * Clears out all the DrawingAnimals in the game except for the first animal, for which it resets
   * its position and clears its commands. Assumes that there is at least one animal active in the
   * game at the time when it is cleared.
   */
  @Override
  protected void clearOutForNewGame() {
    List<Animal> allAnimals = List.copyOf(getAnimals());
    for (Animal curAnimal : allAnimals) {
      if (curAnimal.getMyAnimalData().getMyID() != 0) {
        curAnimal.getMyAnimalData().setPenStatus(false);
        getAnimals().remove(curAnimal);
      } else {
        curAnimal.clearCommands();
        curAnimal.resetPosition();
      }
    }
  }
}
