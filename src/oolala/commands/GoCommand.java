package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalManager;
import oolala.model.DarwinAnimal;

/**
 * Extends Command to implement functionality for DarwinAnimal to change the index of the next
 * command it will execute. Assumptions: currentAnimal is a DarwinAnimal and animalManager is a
 * DarwinManager, and both exist, and a valid index has been set for the value. Dependent on both
 * DarwinAnimal and DarwinManager, as well as the Controller package to create these commands
 * correctly.
 */
public class GoCommand extends Command {

  /**
   * Executes Go command on current Animal, changing its current index to the specified index
   *
   * @param currentAnimal DarwinAnimal input to be acted on
   * @param animalManager DarwinManager input that controls the animals in the game
   * @return boolean whether the instruction completed an action
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    DarwinAnimal animalDoingAction = (DarwinAnimal) currentAnimal;
    animalDoingAction.setCmdCurrentIndex((int) getMyValue());
    return false;
  }
}
