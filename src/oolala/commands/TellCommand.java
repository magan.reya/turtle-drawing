package oolala.commands;

import java.util.Queue;
import oolala.model.Animal;
import oolala.model.AnimalManager;
import oolala.model.DrawingAnimal;


/**
 * Extends Command to implement functionality for a DrawingAnimal to create another DrawingAnimal in
 * the game who follows the same instructions. Assumptions: currentAnimal is a DrawingAnimal and
 * animalManager is a DrawingManager, and both exist. Dependent on both DrawingAnimal and
 * DrawingManager, as well as the Controller package to create these commands correctly.
 */
public class TellCommand extends Command {

  /**
   * Implements the Tell command by creating a new animal of the same type by calling the
   * .replicate() command and adds all the instructions to this new animal.
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    DrawingAnimal startingAnimal = (DrawingAnimal) currentAnimal;
    Animal newAnimal = startingAnimal.replicate((int) getMyValue());
    newAnimal.addCommands((Queue) startingAnimal.getCommandsToExecute());
    animalManager.getAnimals().add(newAnimal);
    return true;
  }
}
