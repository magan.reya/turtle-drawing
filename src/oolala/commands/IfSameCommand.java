package oolala.commands;

import java.util.List;
import oolala.model.Animal;
import oolala.model.AnimalManager;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;


/**
 * Extends Command to implement functionality for DarwinAnimal to change the index of the next
 * command it will execute if an animal of the same species is around. Assumptions: currentAnimal is
 * a DarwinAnimal and animalManager is a DarwinManager, and both exist, and a valid index has been
 * set for the value. Dependent on both DarwinAnimal and DarwinManager, as well as the Controller
 * package to create these commands correctly.
 */
public class IfSameCommand extends Command {

  /**
   * Execute command if there exists an animal of the same species in currentAnimal's vicinity.
   * Assumed that a valid index is set for the value.
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    DarwinAnimal startingAnimal = (DarwinAnimal) currentAnimal;
    DarwinManager darwinManager = (DarwinManager) animalManager;
    List<Animal> surroundingAnimals = darwinManager.getOthersAround(currentAnimal);
    for (Animal closeAnimal : surroundingAnimals) {
      if (startingAnimal.getMyAnimalData().getMyType()
          .equals(closeAnimal.getMyAnimalData().getMyType()) && startingAnimal != closeAnimal) {
          startingAnimal.setCmdCurrentIndex((int) getMyValue());
          return false;
      }
    }
    return false;
  }

}
