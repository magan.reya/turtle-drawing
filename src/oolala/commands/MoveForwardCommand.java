package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalManager;

/**
 * Extends Command to implement functionality for an Animal to move forwards. Assume that
 * currentAnimal and AnimalManager both exist. Dependent on Animal having a correct move method
 * implementation, as well as the Controller package being able to create these commands correctly.
 */
public class MoveForwardCommand extends Command{
    /**
     * Call move on the current animal with the given value to move forwards a given distance
     * (the value of the command)
     *
     * @param currentAnimal current animal passed to act up on
     * @param animalManager the game's animal manager that knows about and can edit all animals in the
     *                      game
     * @return boolean whether the instruction has been completed
     */
    @Override
    public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
        currentAnimal.move(this.getMyValue());
        return true;
    }

}
