package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalManager;

/**
 * Extends Command to implement functionality for an Animal to turn right. Assume that currentAnimal and
 * AnimalManager both exist. Dependent on Animal having a correct turn method implementation, as
 * well as the Controller package being able to create these commands correctly.
 */
public class TurnRightCommand extends Command {

  public TurnRightCommand() {
    super();
  }

  /**
   * orients currentAnimals this.getMyValue degrees to the left
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    currentAnimal.rotate(this.getMyValue());
    return true;
  }
}
