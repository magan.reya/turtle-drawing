package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalManager;

/**
 * The command class is a representation of a command that will be applied to an Animal, and will
 * cause it to take an action. This abstract class is the general format for commands, and
 * can be extended to create commands with specific functionality. This class may fail if the
 * incorrect type of Animal is passed to it, or if there is not an AnimalManager passed to it. It
 * is dependent on having an Animal and AnimalManager passed to it that it can act upon. An
 * example of using this class would be to extend it to create a command with specific
 * functionality, like the MoveForward command, and someone could implement the executeInstruction
 * method for that class to do the specified functionality.
 */

public abstract class Command {



  private double myValue;

  /**
   * Executes command on current animal, makes a change to that particular animal/animal manager. We
   * assume that there's an Animal and an Animal Manager that have been passed, and that the
   * specified functionality for the command has been implemented correctly
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  public abstract boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager);

  /**
   * Sets the value passed as the command's value stored. Must pass a valid value, can break if an
   * incorrect value is passed for a given command.
   *
   * @param value value to set the command to
   */
  public void setMyValue(double value) {
    myValue = value;
  }

  /**
   * Returns the value that the command has stored. Could break and cause problems if an invalid
   * value is stored.
   *
   * @return the command's value
   */
  public double getMyValue() {
    return myValue;
  }

  /**
   * checks if two commands are equal (not physical address in memory). could fail if object o
   * passed could not be cast to a command object.
   *
   * @param o object passed to check, in this case it is another command
   * @return boolean denoting whether two commands are the same
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Command command = (Command) o;
    return Double.compare(command.myValue, myValue) == 0;
  }

}
