package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalData;
import oolala.model.AnimalManager;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;


/**
 * Extends Command to implement functionality for DarwinAnimal to change the index of the next
 * command it will execute if its surroundings are empty. Assumptions: currentAnimal is a
 * DarwinAnimal and animalManager is a DarwinManager, and both exist, and a valid index has been set
 * for the value. Dependent on both DarwinAnimal and DarwinManager, as well as the Controller
 * package to create these commands correctly.
 */
public class IfEmptyCommand extends Command {


  /**
   * Checks if the area around an animal is empty (no walls and no animals around), if so, execute
   * the command at the given index next. Assumed that a valid index is set for the value. Also
   * assumes that methods in AnimalManager and currentAnimal are working correctly to provide
   * information about walls and other animals around.
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    DarwinAnimal animalDoingAction = (DarwinAnimal) currentAnimal;
    DarwinManager darwinManager = (DarwinManager) animalManager;
    AnimalData curAnimalData = animalDoingAction.getMyAnimalData();
    double currentX = curAnimalData.getMyX();
    double currentY = curAnimalData.getMyY();
    if (!animalDoingAction.wallNearMe(currentX, currentY)
        && darwinManager.getOthersAround(currentAnimal).isEmpty()) {
      animalDoingAction.setCmdCurrentIndex((int) getMyValue());
    }
    return false;
  }
}
