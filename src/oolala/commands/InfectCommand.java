package oolala.commands;

import java.util.List;
import oolala.model.Animal;
import oolala.model.AnimalManager;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;

/**
 * Extends Command to implement functionality for a DarwinAnimal infect other animals and change
 * their type and instruction set to its own. Assumptions: currentAnimal is a DarwinAnimal and
 * animalManager is a DarwinManager, and both exist. Dependent on both DarwinAnimal and
 * DarwinManager, as well as the Controller package to create these commands correctly.
 */
public class InfectCommand extends Command {

  /**
   * Checks if the area around an animal contains other animals of a different type, if so, infect
   * them and change their type and commands. Also assumes that methods in AnimalManager and
   * currentAnimal are working correctly to provide information about other animals around.
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    DarwinAnimal startingAnimal = (DarwinAnimal) currentAnimal;
    DarwinManager darwinManager = (DarwinManager) animalManager;
    List<Animal> victimAnimals = darwinManager.getOthersAround(currentAnimal);
    for (Animal victim : victimAnimals) {
      if (!victim.getMyAnimalData().getMyType()
          .equals(startingAnimal.getMyAnimalData().getMyType())) {
        startingAnimal.infect(List.of(victim));
      }
    }
    return true;
  }
}
