package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalData;
import oolala.model.AnimalManager;
import oolala.model.DarwinAnimal;


/**
 * Extends Command to implement functionality for DarwinAnimal to change the index of the next
 * command it will execute if a wall is nearby. Assumptions: currentAnimal is a DarwinAnimal and
 * animalManager is a DarwinManager, and both exist, and a valid index has been set for the value.
 * Dependent on both DarwinAnimal and DarwinManager, as well as the Controller package to create
 * these commands correctly.
 */
public class IfWallCommand extends Command {

  /**
   * Checks if the area around an animal does not contain walls, if so, execute the command at the
   * given index next. Assumed that a valid index is set for the value. Also assumes that methods in
   * AnimalManager and currentAnimal are working correctly to provide information about walls around.
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    DarwinAnimal animalDoingAction = (DarwinAnimal) currentAnimal;
    AnimalData curAnimalData = animalDoingAction.getMyAnimalData();
    double currentX = Math.abs(curAnimalData.getMyX());
    double currentY = Math.abs(curAnimalData.getMyY());
    if (animalDoingAction.wallNearMe(currentX, currentY)) {
      animalDoingAction.setCmdCurrentIndex((int) getMyValue());
    }
    return false;
  }
}


