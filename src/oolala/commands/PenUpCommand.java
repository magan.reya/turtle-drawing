package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalManager;
import oolala.model.DrawingAnimal;


/**
 * Extends Command to implement functionality for a DrawingAnimal to allow it to not draw on the
 * screen. Assumptions: currentAnimal is a DrawingAnimal and animalManager is a DrawingManager, and
 * both exist. Dependent on both DrawingAnimal and DrawingManager, as well as the Controller package
 * to create these commands correctly.
 */
public class PenUpCommand extends Command{
    public PenUpCommand() { super();}


    /**
     *  Makes currentAnimal not draw a line while moving by calling penUp() on the given animal.
     *
     * @param currentAnimal current animal passed to act up on
     * @param animalManager the game's animal manager that knows about and can edit all animals in the
     *                      game
     * @return boolean whether the instruction has been completed
     */
    @Override
    public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
        DrawingAnimal startingAnimal = (DrawingAnimal) currentAnimal;
        startingAnimal.penUp();
        return true;
    }
}
