package oolala.commands;

import java.util.Random;
import oolala.model.Animal;
import oolala.model.AnimalManager;
import oolala.model.DarwinAnimal;

/**
 * Extends Command to implement functionality for DarwinAnimal to change the index of the next
 * command half of the time. Assumptions: currentAnimal is a DarwinAnimal and animalManager is a
 * DarwinManager, and both exist, and a valid index has been set for the value. Dependent on both
 * DarwinAnimal and DarwinManager, as well as the Controller package to create these commands
 * correctly.
 */
public class IfRandomCommand extends Command {

  /**
   * Execute the command at the given command value index next half of the time, and the other half of the
   * time, simply move to the next command.
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    DarwinAnimal animalDoingAction = (DarwinAnimal) currentAnimal;
    Random r = new Random();
    if (r.nextInt(2) == 1) {
      animalDoingAction.setCmdCurrentIndex((int) getMyValue());
    }
    return false;
  }
}
