package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalManager;


/**
 * Extends Command to implement functionality for an Animal to move back to its home position. Assume that
 * currentAnimal and AnimalManager both exist. Dependent on Animal having a correct resetPosition method
 * implementation, as well as the Controller package being able to create these commands correctly.
 */
public class ResetPositionCommand extends Command {

  public ResetPositionCommand() {
    super();
  }

  /**
   * Makes currentAnimal return to specified home position on screen by calling .resetPosition() on the given animal.
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  @Override
  public boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager) {
    currentAnimal.resetPosition();
    return true;
  }

}
