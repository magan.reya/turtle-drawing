package oolala.view;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import oolala.view.factories.ImageFileFactory;
import oolala.view.factories.SliderFactory;
import oolala.view.factories.TextFactory;

import java.io.File;
/**
 * This class extends the GameView class to add extra JavaFX elements that are unique only to
 * the Darwin Application. It creates these elements using factories and event calls.
 */
public class DarwinGameView extends GameView{


    private static final int LOAD_FILE = 1;
    private static final int SET_HOME = 1;
    private static final int PEN_COLOR = 1;
    private Slider slider;
    private ImageFileFactory myImage = new ImageFileFactory();
    private SliderFactory mySlider = new SliderFactory();

    /**
     * Constructor for DarwinGameView. It has the same parameters passed in as GameView
     * @param myGame: Darwin
     * @param myLanguage: selected Language to play in
     */
    public DarwinGameView(String myGame, String myLanguage){
        super(myGame, myLanguage);
    }

    @Override
    protected Node makeUserInputs(){
        HBox top = (HBox) super.makeUserInputs();
        top.getChildren().remove(LOAD_FILE);
        top.getChildren().remove(SET_HOME);
        top.getChildren().remove(PEN_COLOR);
        Button newSpecies = getMyButton().makeButton(getButtonDisplays(),"Species", event -> addSpecies());
        Button playAnimation = getMyButton().makeButton(getButtonDisplays(), "Play", event -> playEvent());
        top.getChildren().addAll(newSpecies, playAnimation);
        return top;
    }

    @Override
    protected Node makeBottomPanel(){
        Button changeRadius = getMyButton().makeButton(getButtonDisplays(), "radius", event -> radiusChange());
        slider = mySlider.setSlider();
        BorderPane bp = new BorderPane();
        bp.setCenter(slider);
        bp.setRight(changeRadius);
        return bp;
    }

    @Override
    protected Node makeVerticalPanel(){
        VBox right = (VBox) super.makeVerticalPanel();
        Label instructions = getMyTextField().makeTitle(getTitleDisplays(), "Darwin");
        right.getChildren().add(instructions);
        return right;
    }

    private void addSpecies(){
        FileChooser imgFile = myImage.changeImage(getInputDisplays().getString("AddSpecies"));
        try{
            File imgFileString = imgFile.showOpenDialog(new Stage());
            String[] animalName = imgFileString.getName().split("\\.");
            getMyController().setupAnimals(animalName[0]);
            myAnimation.pause();
            getMyController().showInView();
            addInsts();
        }
        catch(Exception e){return;}
    }

    private void addInsts(){
        getMyCommandEvents().loadCommandFile(getTitleDisplays(), getErrorDisplays(), getMyHistory(), getInputDisplays(), "Darwin", getMyController());
    }

    private void radiusChange(){
        getMyController().animalRadiusChange(slider.getValue());

    }

    private void playEvent(){
        myAnimation.play();

    }


    /**
     * Abstract method from GameView. No special initialization is needed for Darwin so it returns null
     * @return null
     */
    @Override
    public String setUpSystem(){return null;}
}

