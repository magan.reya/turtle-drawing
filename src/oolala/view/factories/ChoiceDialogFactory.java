package oolala.view.factories;

import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceDialog;

import java.util.ResourceBundle;

public class ChoiceDialogFactory {

    /**
     * This method/class makes all the ChoiceDialogs nodes seen on the scene, the user is prompted to choose from a list
     * of options to start a game. This is called to set the game type and language within the game.
     * @param input: This is the key that maps to either a game selection or language selection within the choice
     *             resource bundle.
     * @param choice: This is the resource bundle that has the list of choices that
     *             can be found within the choice dialog. The resource bundles ChoiceDialogText and Languages
     *              are passed through here.
     * @return mySelection: a ChoiceDialog that has all of the appropriate selectable options (either games or languages)
     */
    public ChoiceDialog<String> makeChoiceDialog(String input, ResourceBundle choice){
        ChoiceDialog<String> mySelection = new ChoiceDialog<>();
        mySelection.setContentText(input);
        ObservableList<String> games  = mySelection.getItems();
        for(String s: choice.keySet()){
            games.add(choice.getString(s));
        }
        return mySelection;
    }

}
