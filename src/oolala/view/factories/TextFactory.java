package oolala.view.factories;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.util.ResourceBundle;

public class TextFactory {
    /**
     * This class deals with the creation of basic Text related JavaFX objects within the game scene.
     * It creates both a TextField for text insertion, as well as labels to display relevant game instructions.
     * It assumes the TextField/Instructions to display are the ones that are passed in as keys to resource bundles.
     * It will assume that these passed in values are the correct games currently being played.
     */

    /**
     * This method makes a textfield to be displayed at the bottom borderpane in the game scene.
     * @return new TextField() that has been created.
     */
    public TextField makeTextField(){
        return new TextField();
    }

    /**
     * This method creates the instruction set labels that are displayed on the game scene.
     * @param titleDisplays: This resource bundle contains all of the relevant labels (titles, error titles)
     * @param text: This is the key that maps to the correct instructions (the key is either Logo, LSystem,
     *            or Darwin) within the titleDisplays resource bundle.
     * @return new Label, with the content text of the relevant instructions.
     */
    public Label makeTitle (ResourceBundle titleDisplays, String text) {
        return new Label(titleDisplays.getString(text));
    }
}
