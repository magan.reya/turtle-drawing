package oolala.view.factories;

import javafx.scene.control.ChoiceDialog;

public class SelectionFactory {
    /**
     * This method/class chooses a selection from a choice dialog pop up. When this method is called,
     * the passed in choice dialog waits for the user to select from the list of options, and then
     * returns the selected option.
     * @param selectionBox: This is the choice dialog that has the relevant list of options to select from
     * @return mySelected: A string that was selected from the options within selectionBox.
     */
    public String selectChoice(ChoiceDialog<String> selectionBox) {
        selectionBox.showAndWait();
        if (selectionBox.getSelectedItem() == null) {
            selectChoice(selectionBox); //okay to be recursive?
        }
        return selectionBox.getSelectedItem();
    }
}
