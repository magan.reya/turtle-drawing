package oolala.view.factories;

import javafx.stage.FileChooser;

import java.io.File;

public class ImageFileFactory {

    /**
     * This method/class creates a FileChooser to open up the images directory within the OOLALA
     * project and allow users to select images to display from it.
     * @param value: This is the given value that is returned when a relevant key is passed to
     *             a resource bundle. For example, when adding a new species, the "AddSpecies"
     *             key is passed to get the prompt "Insert New Species" on the Image File Chooser.
     * @return imgFile: The file chooser for images to be displayed on the scene.
     */
    public FileChooser changeImage(String value){
        FileChooser imgFile = new FileChooser();
        imgFile.setTitle(value);
        imgFile.setInitialDirectory(new File("images/"));
        return imgFile;
    }
}
