package oolala.view.factories;

import javafx.scene.control.TextInputDialog;
import oolala.view.events.ErrorPopUps;

import java.util.ResourceBundle;

public class SystemFactory {
    ErrorPopUps myErrors = new ErrorPopUps();

    /**
     * This method/class sets up the system for a game. It is primarily used by LSystem.
     * When a user starts the LSystem game, this method is called to ask the user to input
     * the length, width, and height for the LSystem game. Errors can happen here if nothing is inputted,
     * at which point the user is asked again to input parameters.
     * @param titleDisplays: This resource bundle contains all of the relevant labels (titles, error titles)
     * @param content: This is the string that is the key to the text to be displayed from the inputDisplay resource
     *               bundle. For example for LSystem, content: getInputDisplays().getString("LSystem"). Which
     *               maps to the prompt for the user to insert relevant LSystem parameters.
     * @param error: This is the error key that matches to a specific error in the ErrorTextDisplay ResourceBundles
     *             which is sent to the ErrorPopUps() class.
     * @return systemValues.getEditor().getText(): The string of parameters that the user inputted to set
     * up the game system.
     */
    public String createSystem(ResourceBundle titleDisplays, String content, String error){
        TextInputDialog systemValues = new TextInputDialog();
        systemValues.setContentText(content);
        systemValues.showAndWait();
        if(systemValues.getEditor().getText() == null){
            myErrors.showError(titleDisplays, error);
            createSystem(titleDisplays, content, error);
        }
        return systemValues.getEditor().getText();
        }
    }

