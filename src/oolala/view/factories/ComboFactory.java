package oolala.view.factories;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;

import java.util.ResourceBundle;

public class ComboFactory {

    /**
     * This method/class makes all the ComboBox node seen on screen, and maps it to an ActionEvent. When
     * a value is selected from the drop down of the ComboBox, the ActionEvent is triggered to access past
     * commands
     * @param comboTextDisplays: This is the resource bundle for the title of the ComboBox
     * @param label: This is the key that maps to the title within comboTextDisplays
     * @param handler: This is the ActionEvent that is triggered when a user selects from the ComboBox. It loads
     *               up a past command and sends it to be processed in the controller.
     * @return historyCombo: This is the created ComboBox that has the history of past commands.
     */
    public ComboBox<String> makeComboBox(ResourceBundle comboTextDisplays, String label, EventHandler<ActionEvent> handler){
        ComboBox<String> historyCombo = new ComboBox<>();
        historyCombo.setPromptText(comboTextDisplays.getString(label));
        historyCombo.setId("COMBO");
        historyCombo.setOnAction(handler);
        return historyCombo;
    }
}
