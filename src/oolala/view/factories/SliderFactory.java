package oolala.view.factories;

import javafx.scene.control.Slider;

public class SliderFactory {

    private static final int MIN = 10;
    private static final int MAX = 70;
    private static final int VALUE = 10;
    private static final int TICK_UNIT = 5;

    /**
     * This method/class creates a slider for Darwin so that a user can change the radius
     * of enemy detection bounds.
     * @return slider: The newly created Darwin slider.
     */
    public Slider setSlider(){
        Slider slider = new Slider(MIN, MAX, VALUE);
        slider.setMajorTickUnit(TICK_UNIT);
        slider.setId("SLIDE");
        slider.setSnapToTicks(true);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        return slider;
    }

}
