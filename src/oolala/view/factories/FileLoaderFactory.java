package oolala.view.factories;

import java.io.File;
import java.util.ResourceBundle;


public class FileLoaderFactory {
    private static final String FILE_PATH_RESOURCE_BUNDLE ="oolala.view.resources.FilePathChoice";

    /**
     * This method/class creates the file directory to be loaded in the file chooser
     * based on the game that has been selected. It loads up the file path resource bundle
     * and takes the file string to load the correct directory.
     * For example, "Logo" will load data/examples/logo/.
     * @param file: the key that maps to the correct directory path in the file path resource bundle
     * @return File: the correct directory for the FileChooser
     */
    public File fileDirectory(String file){
        ResourceBundle filePath = ResourceBundle.getBundle(FILE_PATH_RESOURCE_BUNDLE);
        return new File(filePath.getString(file));
    }
}
