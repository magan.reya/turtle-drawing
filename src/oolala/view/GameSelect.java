package oolala.view;

import javafx.scene.control.ChoiceDialog;
import oolala.view.factories.ChoiceDialogFactory;
import oolala.view.factories.SelectionFactory;

import java.util.ResourceBundle;
/**
 * This class is called right when the game is started to choose the Game to play
 * (Logo, LSystem, or Darwin) and the Language to play it in (English, Spanish, French).
 */
public class GameSelect {

    private static final String RESOURCE_PACKAGE = "oolala.view.resources.";
    private ChoiceDialog<String> myGameSelection;
    private ChoiceDialog<String> myLanguageSelection;
    private ChoiceDialogFactory myChoiceDialog = new ChoiceDialogFactory();
    private static final String CHOICE_DISPLAY_RESOURCE_BUNDLE = "ChoiceDialogText";
    private static final String INPUT_DIALOG_TEXT_DISPLAY = "InputDialogTextDisplayEnglish";
    private static final String LANGUAGE_DISPLAY = "Languages";
    private String mySelectedGame;
    private String myLanguage;
    private GameView myGameView;
    private SelectionFactory mySelection = new SelectionFactory();
    private ResourceBundle languageDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + LANGUAGE_DISPLAY);
    private ResourceBundle inputDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + INPUT_DIALOG_TEXT_DISPLAY);
    private ResourceBundle choiceDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + CHOICE_DISPLAY_RESOURCE_BUNDLE);

    private void selectGame() {
        mySelectedGame = mySelection.selectChoice(myGameSelection);
        setMyGameSelection();
    }
    private void selectLanguage(){
        myLanguage = mySelection.selectChoice(myLanguageSelection);
    }

    /**
     * This method pops up a Choice Dialog with options of the three games, and the user is prompted to select
     * from these games. The selected game is passed into the GameView class and is shown once the scene is made.
     * @return GameView object with correct game (LogoGameView, LSystemGameView, or DarwinGameView)
     */
    public GameView makeGameDisplay() {
        myGameSelection = myChoiceDialog.makeChoiceDialog(inputDisplays.getString("SelectGame"), choiceDisplays);
        selectGame();
        return myGameView;
    }

    /**
     * This method pops up a Choice Dialog with options of three languages to choose from, and the user
     * is prompted to select one of these languages. The selected language is passed into GameView, and the
     * game runs in this language.
     * @return selected language
     */
    public String makeLanguage(){
        myLanguageSelection = myChoiceDialog.makeChoiceDialog(inputDisplays.getString("Language"), languageDisplays);
        selectLanguage();
        return myLanguage;
    }


    private void setMyGameSelection(){
        if(mySelectedGame.equals(choiceDisplays.getString("LSystem"))){
            myGameView = new LSystemGameView(mySelectedGame, myLanguage);
        }
        if(mySelectedGame.equals(choiceDisplays.getString("Logo"))){
            myGameView = new LogoGameView(mySelectedGame, myLanguage);
        }
        if(mySelectedGame.equals(choiceDisplays.getString("Darwin"))){
            myGameView = new DarwinGameView(mySelectedGame, myLanguage);
        }
    }



}
