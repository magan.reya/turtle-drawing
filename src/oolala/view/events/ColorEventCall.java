package oolala.view.events;

import javafx.scene.control.ChoiceDialog;
import oolala.view.factories.ChoiceDialogFactory;

import java.util.ResourceBundle;

public class ColorEventCall {

  /**
   * This method/class allows the user to choose a color from a drop down menu of options, and set it either
   * as the pen color or background color.
   * @param inputDisplay: This resource bundle contains the dialogs for all input displays that show up (dialogs that prompt
   *                        a user to load a file)
   * @param colorChoices: ChoiceDialog that has a list of colors to choose from
   * @return myColorSelection.getSelectedItem(): returns selected color from choice dialog
   */
  public String setColor(ResourceBundle inputDisplay, ResourceBundle colorChoices) {
    ChoiceDialogFactory myColorFactory = new ChoiceDialogFactory();
    ChoiceDialog<String> myColorSelection = myColorFactory.makeChoiceDialog(inputDisplay.getString("Color"),
        colorChoices);
    myColorSelection.showAndWait();
    if (myColorSelection.getSelectedItem() == null) {
      setColor(inputDisplay, colorChoices);
    }
    return myColorSelection.getSelectedItem();
  }
}