package oolala.view.events;

import javafx.scene.control.TextInputDialog;

import java.util.ResourceBundle;

public class HomeEventCall {
    ErrorPopUps myErrors = new ErrorPopUps();

    /**
     * This method is triggered by the pressing the Set Default Position. It prompts the user
     * to input the new Home position for the animal within a text input dialog. If the user inputs something
     * invalid or does not input anything, an error will show up to inform them of this.
     * @param titleDisplays: This resource bundle contains all of the relevant labels (titles, error titles)
     * @param inputDisplays:This resource bundle contains the dialogs for all input displays that show up (dialogs that prompt
     *                       a user to load a file)
     * @param errorDisplays: This resource bundle contains possible errors (Invalid File, Invalid Parameter)
     * @return newHomeValues.getEditor().getText(), which is the new home value string
     */
    public String getHomeInputValues(ResourceBundle titleDisplays, ResourceBundle inputDisplays, ResourceBundle errorDisplays){
    TextInputDialog newHomeValues = new TextInputDialog();
    newHomeValues.setContentText(inputDisplays.getString("HomeInput"));
    newHomeValues.showAndWait();
    if(newHomeValues.getEditor().getText() == null){
        myErrors.showError(titleDisplays, errorDisplays.getString(errorDisplays.getString("newHomeError")));
    }
    return newHomeValues.getEditor().getText();
}


}
