package oolala.view.events;

import javafx.scene.control.Alert;

import java.util.ResourceBundle;
import javafx.scene.control.Alert.AlertType;

public class ErrorPopUps {

    /**
     * This method/class shows the relevant error to the user. It is called for any exception that is caught, and
     * will display the appropriate response to the user. For example an invalid instruction like fdd will yield
     * an invalid instruction error display when attempted during the game.
     * @param titleDisplays: This resource bundle contains all of the relevant labels (titles, error titles)
     * @param message: This is the key for the error title within the titleDisplays resource bundle. It maps
     *               to OOLALA ERROR.
     */
    public void showError (ResourceBundle titleDisplays, String message) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(titleDisplays.getString("error"));
        alert.setContentText(message);
        alert.showAndWait();
    }
}
