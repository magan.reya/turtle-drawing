package oolala.view.events;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import oolala.controller.GameController;
import oolala.view.factories.FileLoaderFactory;

import java.io.File;
import java.util.List;
import java.util.ResourceBundle;

public class CommandEventCalls {
    private ComboBox<String> myHistory;
    private final FileLoaderFactory myFileLoader = new FileLoaderFactory();
    private final ErrorPopUps myErrors = new ErrorPopUps();

    /**
     * This method is triggered when the user presses the load command button.
     * Initiates a file chooser for command files within applications. User is prompted to select
     * a file and then this file is processed within the controller to get commands to run. The errors
     * that can occur during this process happen if a user tries to load a file from one game during another.
     * For example, loading a LSystem game file within a Logo game will cause an error to display.
     * @param titleDisplays: This resource bundle contains all of the relevant labels (titles, error titles)
     * @param errorDisplays: This resource bundle contains possible errors (Invalid File, Invalid Parameter)
     * @param history: This ComboBox has a list of the previous instructions, each time a new instruction is run it is added
     *               to this ComboBox.
     * @param inputDisplays: This resource bundle contains the dialogs for all input displays that show up (dialogs that prompt
     *                     a user to load a file)
     * @param mySelectedGame: This is the user selected game (Logo, LSystem, or Darwin). The file chooser will open the directory
     *                      of the chosen game.
     * @param myController: This is the controller within the game to which the commands are sent to for parsing.
     */
    public void loadCommandFile(ResourceBundle titleDisplays, ResourceBundle errorDisplays, ComboBox<String> history, ResourceBundle inputDisplays, String mySelectedGame, GameController myController) {
        myHistory = history;
        FileChooser commandFile = new FileChooser();
        commandFile.setTitle(inputDisplays.getString("LoadFile"));
        File filePath = myFileLoader.fileDirectory(mySelectedGame);
        commandFile.setInitialDirectory(filePath);
        try {
            File commandFileString = commandFile.showOpenDialog(new Stage());
            List<String> isnListFromFile = myController.createListOfIsnsFromFile(commandFileString.toString());
            for (String s : isnListFromFile) {
                addToHistory(s);
            }
            myController.createCommands(isnListFromFile);
        }
        catch (Exception e){
            myErrors.showError(titleDisplays, errorDisplays.getString("InvalidLoad"));
       }
    }

    /**
     * This method is triggered when a user selects a command from the history ComboBox.
     * This method allows the user to rerun commands that were previously run. The user can click on a
     * command from the ComboBox drop down menu and it will automatically run. The assumption is that the user
     * will have already ran a command before trying to run an old one. The list starts off as empty, so
     * commands won't be added until they have been ran.
     * @param history: This is the ComboBox that has all of the previously ran commands. Users can select a command from the
     *               list to rerun it.
     * @param myController: This is the controller within a game, once the previous command is selected from the ComboBox,
     *                    it is passed to the controller so that the command can be processed and created.
     */
    public void visitOldCommands(ComboBox<String> history, GameController myController){
        myHistory = history;
        if(history.getValue() != null){
            myController.createCommands(List.of(history.getValue()));
            addToHistory(history.getValue());
        }
    }

    /**
     * This method is triggered by the press of the Add Command button on the bottom border pane of the game
     * screen. This method then takes the text written in the text field within the border pane and sends it to
     * the controller for command processing and creation. The errors that can show up here are when the user
     * types in a command that is invalid for the game that is being played.
     * @param history: This is the ComboBox that has all of the previously ran commands. Users can select a command from the
     *                 list to rerun it.
     * @param myAddedCommands: This is the textfield to which a user can type in the commands they wish to run.
     * @param myController:This is the controller within a game, once the previous command is selected from the ComboBox,
     *                   it is passed to the controller so that the command can be processed and created.
     */
    public void addCommand(ComboBox<String> history, TextField myAddedCommands, GameController myController){
        myHistory = history;
        String newCommandString = myAddedCommands.getText();
        List<String> commandsList = myController.createListOfIsnsFromInput(List.of(newCommandString));
        myController.createCommands(commandsList);
        for(String s : commandsList){
            addToHistory(s);
        }
        myAddedCommands.clear();
    }

    private void addToHistory(String oldCommand){
        myHistory.getItems().add(oldCommand);
    }
}
