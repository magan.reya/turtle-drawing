package oolala.view.events;

import javafx.scene.Group;
import javafx.scene.control.ComboBox;
import oolala.controller.GameController;
import oolala.view.AnimalDisplay;

public class NewGameEventCalls {
    private static final int LINE_START = 2;
    private String mySelectedGame;

    /**
     * This method is called every time a new game is selected. It communicates
     * with the game controller to create the relevant translator class for each game type.
     * @param myController: This is the controller within the game to which the commands are sent to for parsing.
     *                    The controller receives the selected game and loads up a command translator for that game.
     * @param selectedGame: This is the user selected game (Logo, LSystem, or Darwin). The file chooser will open the directory
     *                       of the chosen game.
     */
    public void selectGame(GameController myController, String selectedGame){
        mySelectedGame = selectedGame;
        myController.setMyClassesForGameType(selectedGame);
    }

    /**
     * This is triggered when the new game button is pressed. It allows the user to restart the current game they
     * are playing (if playing Logo, you can restart Logo). Everything is reset when this event is called.
     * @param myHistory: This ComboBox has a list of the previous instructions, each time a new instruction is run it is added
     *                   to this ComboBox. It is cleared at the start of a new game.
     * @param gameDisplay: This is the group that contains all of the nodes that are displayed on the scene.
     *                   The animals and lines are removed at the start of a new game.
     * @param myAnimalDisplay: This is an AnimalDisplay object which deals with the ImageView for the game. It deletes the ImageView
     *                       (the image is added at the start of a new game).
     * @param myController: This is the controller within the game to which the commands are sent to for parsing. The controller is reset
     *                    to have no commands and no previous data at the start of each new game.
     */
    public void newGame(ComboBox<String> myHistory, Group gameDisplay, AnimalDisplay myAnimalDisplay, GameController myController){
        myHistory.getItems().clear();
        gameDisplay.getChildren().remove(LINE_START, gameDisplay.getChildren().size());
        myAnimalDisplay.clearAll(gameDisplay);
        myController.resetScene();
        selectGame(myController, mySelectedGame);
    }
}
