package oolala.view;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import oolala.model.AnimalData;
import oolala.view.events.ErrorPopUps;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * This class deals with actually displaying the ImageView animal images on the screen, as well as
 * all of the line drawings that are seen on the screen. This class is called often by the GameView to set
 * up images, their positions, and their visibility. This is the class that is associated with displaying
 * a lot of the command changes when a command is run. For example, after running "fd 50" it is this class
 * that will be called to show the change in position of the ImageView as well as the drawing of the line.
 * This class also allows for manipulation of the line drawing to change colors and width. This class assumes
 * that an ImageView is in play and changing as commands are called.
 */
public class AnimalDisplay {

  private static final int INITIAL_POSITION = 200;
  private static final int STAMP_HEIGHT = 40;
  private static final int STAMP_WIDTH = 30;
  private double strokeWidth = 1;
  private Paint strokeColor = Color.BLACK;
  private String newImagePath = "turtle";
  private boolean imageChange = false;
  private String addImagePath;
  private boolean addImage = false;

  private Map<Integer, ImageView> idMap;
  private Map<String, String> fileMap;
  private static final String TURTLE_IMAGE_FILE_PATH = "images/turtle.png";
  private static final String ELEPHANT_IMAGE_FILE_PATH = "images/elephant.png";
  private static final String TIGER_IMAGE_FILE_PATH = "images/tiger.png";
  private static final String LEAF_IMAGE_FILE_PATH = "images/leaf.jpg";

  private final ErrorPopUps myErrors;
  private ResourceBundle errorDisplays;
  private ResourceBundle titleDisplays;

  /**
   * This is the constructor that sets up AnimalDisplay. It sets up the error class to be called
   * anytime an Exception is caught. It also sets up an idMap that maps Animal IDs to their corresponding
   * ImageView objects, and a file map that matches an animal title (for example "Turtle") to its corresponding
   * file path (TURTLE_IMAGE_FILE_PATH).
   */
  public AnimalDisplay() {
    myErrors = new ErrorPopUps();
    idMap = new HashMap<>();
    fileMap = new HashMap<>();
    fillFileMap();
  }

  protected void setResources(ResourceBundle errors, ResourceBundle titles){
    errorDisplays = errors;
    titleDisplays = titles;
  }

  protected void updateFromData(AnimalData newData, Group root) {
    if (!idMap.containsKey(newData.getMyID()) && fileMap.containsKey(newData.getMyType())) {
      addNewAnimalFromData(newData, root, INITIAL_POSITION, INITIAL_POSITION);
      return;
    }
    ImageView animalToUpdate = idMap.get(newData.getMyID());

    double pastX = animalToUpdate.getBoundsInLocal().getCenterX();
    double pastY = animalToUpdate.getBoundsInLocal().getCenterY();

    updateAnimalLocationAndVisibility(newData, animalToUpdate);
    resetImage(newData.getMyType(), animalToUpdate);
    checkAndChangeImage(animalToUpdate);

    if (newData.getStamp()) {
      stampTurtle(newData, root, animalToUpdate, newImagePath);
    }
    if (newData.getPenStatus()) {
      drawLine(root, animalToUpdate, pastX, pastY);
    }
    if(addImage){
      stampTurtle(newData, root, animalToUpdate, addImagePath);
    }
  }

  private void drawLine(Group root, ImageView animalToUpdate, double pastX, double pastY) {
    double newX = animalToUpdate.getBoundsInLocal().getCenterX();
    double newY = animalToUpdate.getBoundsInLocal().getCenterY();
    if (pastX != newX || pastY != newY) {
      Line l = new Line(pastX, pastY, newX, newY);
      l.setStrokeWidth(strokeWidth);
      l.setStroke(strokeColor);
      root.getChildren().add(l);
    }
  }

  protected void setStrokeColor(Color newColor){
    strokeColor = newColor;
  }

  private void resetImage(String type, ImageView animalToUpdate) {
    try {
      animalToUpdate.setImage(new Image(new FileInputStream(fileMap.get(type))));
    } catch (FileNotFoundException e) {
      myErrors.showError(titleDisplays, errorDisplays.getString("ImageLoad"));
    }
  }

  protected void changeImage(String file){
    newImagePath = file;
    imageChange = true;
  }

  protected void checkAndChangeImage(ImageView animalToUpdate){
    if(imageChange){
      resetImage(newImagePath, animalToUpdate);
    }
  }

  protected void addImageToDrawing(String file){
    addImagePath = file;
    addImage = true;
  }


  private void addNewAnimalFromData(AnimalData newData, Group root, int xPos, int yPos) {
    try {
      ImageView animalToUpdate = new ImageView(
          new Image(new FileInputStream(fileMap.get(newData.getMyType()))));
      idMap.put(newData.getMyID(), animalToUpdate);
      animalToUpdate.setX(xPos);
      animalToUpdate.setY(yPos);
      animalToUpdate.setRotate(270);
      animalToUpdate.setFitHeight(20);
      animalToUpdate.setFitWidth(20);
      root.getChildren().add(animalToUpdate);
    } catch (FileNotFoundException e) {
      myErrors.showError(titleDisplays, errorDisplays.getString("ImageLoad"));
    }
  }

  private void updateAnimalLocationAndVisibility(AnimalData newData, ImageView animalToUpdate) {
    animalToUpdate.setX(newData.getMyX());
    animalToUpdate.setY(newData.getMyY());
    animalToUpdate.setRotate(newData.getAngle() + 180);
    animalToUpdate.setVisible(newData.getShow());
  }

  protected void setLineThickness(double width) {
    strokeWidth = width;
  }


  private void stampTurtle(AnimalData newData, Group root, ImageView animalToUpdate, String imageType) {
    try {
      ImageView stampedAnimal = new ImageView(
          new Image(new FileInputStream(fileMap.get(imageType))));
      stampedAnimal.setX(animalToUpdate.getX());
      stampedAnimal.setY(animalToUpdate.getY());
      stampedAnimal.setRotate(newData.getAngle() + 180);
      stampedAnimal.setFitHeight(STAMP_HEIGHT);
      stampedAnimal.setFitWidth(STAMP_WIDTH);
      root.getChildren().add(stampedAnimal);
    } catch (FileNotFoundException e) {
      myErrors.showError(titleDisplays, errorDisplays.getString("ImageLoad"));
    }
  }

  private void fillFileMap() {
    fileMap.put("turtle", TURTLE_IMAGE_FILE_PATH);
    fileMap.put("tiger", TIGER_IMAGE_FILE_PATH);
    fileMap.put("elephant", ELEPHANT_IMAGE_FILE_PATH);
    fileMap.put("leaf", LEAF_IMAGE_FILE_PATH );
  }

  protected ImageView getAnimal(int id) {
    return idMap.get(id);
  }

  /**
   * This method is called when a new game is initiated, it removes the ImageView from the Group and also clears
   * out the idMap so that there are no more ImageView animal objects present.
   */
  public void clearAll(Group root) {
    root.getChildren().remove(1);
    idMap.clear();
  }


  }
