package oolala.view;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import oolala.view.events.ErrorPopUps;
import oolala.view.factories.ImageFileFactory;

import java.io.File;
/**
 * This class extends the GameView class to add extra JavaFX elements that are unique only to
 * the Logo Application. It creates these elements using factories and event calls.
 */
public class LogoGameView extends GameView{

    private ErrorPopUps myErrors = new ErrorPopUps();
    private ImageFileFactory myImage = new ImageFileFactory();

    /**
     * Constructor for LogoGameView. It has the same parameters passed in as GameView
     * @param myGame: Logo
     * @param myLanguage: selected Language to play in
     */
    public LogoGameView(String myGame, String myLanguage){
        super(myGame, myLanguage);
    }

    @Override
    protected Node makeVerticalPanel(){
        VBox right = (VBox) super.makeVerticalPanel();
        Button currentPosition = getMyButton().makeButton(getButtonDisplays(),"Position", event -> getTurtleLocation());
        Button penThickness = getMyButton().makeButton(getButtonDisplays(),"PenThickness", event -> changeStroke());
        Button changeLogoAnimal = getMyButton().makeButton(getButtonDisplays(),"ChangeAnimal", event -> changeAnimal());
        Label instructions = getMyTextField().makeTitle(getTitleDisplays(), "Logo");
        right.getChildren().addAll(currentPosition, penThickness, changeLogoAnimal, instructions);
        return right;
    }

    private void changeAnimal(){
        FileChooser imgFile = myImage.changeImage(getInputDisplays().getString("ChangeImage"));
        try {
            File imgFileString = imgFile.showOpenDialog(new Stage());
            String[] animalName = imgFileString.getName().split("\\.");
            getMyAnimalDisplay().changeImage(animalName[0]);
        }
        catch(Exception e){return;}
    }
    private void changeStroke(){
        try {
            TextInputDialog newStrokeWidth = new TextInputDialog();
            newStrokeWidth.setContentText(getInputDisplays().getString("StrokeWidth"));
            newStrokeWidth.showAndWait();
            getMyAnimalDisplay().setLineThickness(Double.parseDouble(newStrokeWidth.getEditor().getText()));
        }
        catch(Exception e){
            myErrors.showError(getTitleDisplays(), getErrorDisplays().getString("NotANumber"));
        }
    }

    private void getTurtleLocation(){
        String xPosition = String.valueOf(Math.round(getMyAnimalDisplay().getAnimal(0).getX()));
        String yPosition = String.valueOf(Math.round(getMyAnimalDisplay().getAnimal(0).getY()));
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(getTitleDisplays().getString("information"));
        alert.setContentText(String.format(getTitleDisplays().getString("position"), xPosition, yPosition));
        alert.show();
    }

    /**
     * Abstract method from GameView. No special initialization is needed for Logo so it returns null
     * @return null
     */
    @Override
    public String setUpSystem(){
        return null;
    }

}
