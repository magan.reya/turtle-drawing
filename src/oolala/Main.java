package oolala;
import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import oolala.model.GameModel;
import oolala.view.GameSelect;
import oolala.view.GameView;
import oolala.controller.GameController;
import oolala.view.LSystemGameView;
import oolala.view.LogoGameView;


/**
 * Start the game here.
 */
public class Main extends Application {
    public static final String TITLE = "OOLALA";
    public static final int SIZE_X = 700;
    public static final int SIZE_Y = 600;
    public static final Paint DEFAULT_BACKGROUND = Color.THISTLE;


    @Override
    public void start(Stage stage) {
        GameSelect game = new GameSelect();
        game.makeLanguage();
        GameView view = game.makeGameDisplay();
        GameModel model = new GameModel();
        GameController controller = new GameController(view, model);
        view.setMyController(controller);
        view.start(.25);

        stage.setTitle(TITLE);
        stage.setScene(view.makeScene(SIZE_X, SIZE_Y, DEFAULT_BACKGROUND));
        stage.show();
    }
}
