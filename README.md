OOLALA
====

This project implements a variety of applications that take advantage of drawing using a turtle.

Names: Reya Magan, Kristen Angell, Will Long

### Timeline

Start Date: 9/22/21

Finish Date: 10/12/21

Hours Spent: 50+

### Primary Roles

Kristen: Primarily involved with the backend, setting up Controller to Model integration
specifically through the Command Translator and Generator classes as well as Animal/Animal Data set
up. LSystem and Darwin translations, Logo set up. Helping with view debugging. Created abstract
Command Translator class which is extended by game specific command translator classes that allow
for ease of translation from basic (logo) instructions to Darwin or LSystem.

Reya: Primarily involved with the frontend, setting up Controller to View integration by having an
abstract GameView class with each game type extending the abstract class. Set up some functionality
in AnimalDisplay to monitor ImageView positioning and setup. Helped with backend debugging, added
some functionality for Logo commands. Set up error displays as well as multi-language resource
property files and css styling.

Will: Tell command functionality, adding features to view (change pen color, change background
color). Helped with debugging for frontend and backend components. Set up AnimalDisplay class to
take in ImageView elements and add them to maps for processing throughout game. Setup image id
mechanisms to keep track of multiple ImageView objects.

### Resources Used

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/Slider.html - Slider setup

https://code.makery.ch/blog/javafx-dialogs-official/ - Different types of information pop-ups

https://www.geeksforgeeks.org/javafx-textinputdialog/ - TextInputDialog setup

https://www.geeksforgeeks.org/javafx-alert-with-examples/ -Alert setup

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/ChoiceDialog.html - ChoiceDialog
setup

https://www.geeksforgeeks.org/javafx-filechooser-class/ -FileChooser setup

https://www.baeldung.com/reading-file-in-java - Reading in files

geeksforgeeks.org/mvc-design-pattern/ - MVC design

https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html - Java regular expressions

https://www.geeksforgeeks.org/packages-in-java/ - Packages in Java

https://www.jetbrains.com/help/idea/working-with-code-documentation.html#toggle-rendered-view -
Javadocs

### Running the Program

Main class:

Main.java

Data files needed:

Each game type uses a different data folder containing their command files to run. They are as
follows:

* Logo = data/examples/logo
* LSystem = data/examples/lsystem
* Darwin = data/examples/darwin

Each of the images are found in the images/ folder.

Features implemented:

The following features were implemented for each application:

* Darwin:
    * Add New Species - This feature pops up a FileChooser in which a user can select a new species
      to add to the scene. Once a species is selected, the user is also prompted to select an
      instruction set for the new species.
    * New Game - This feature allows the user to restart the current Darwin game to a new Darwin
      game.
    * Play Animation - This feature can be clicked after species have been added to the scene. It
      runs all of the instructions for each species.
    * Past Commands - This feature contains a list of all of the previous commands that have been
      run in Darwin.
    * Set Background Color - This feature allows the user to change the background color of the game
      display rectangle.
    * New Radius - The user can move around the Slider points and click on the New Radius button to
      set a new radius bound for animal enemy detection.

* Logo:
    * New Game - This feature allows the user to restart the current Darwin game to a new Logo game.
    * Load Command File - This feature allows the user to choose a file from a pop-up FileChooser
      and run it as the commands for the application.
    * Set Default Position - This feature prompts the user to set the "Home" position for the turtle
    * Set Pen Color - This feature allows the user to change the pen color of the line that is drawn
    * Past Commands - This feature contains a list of all of the previous commands that have been
      run in Logo. Users can click and rerun these commands.
    * Set Background Color - This feature allows the user to change the background color of the game
      display rectangle.
    * Where is The Turtle? - This feature lets the user know the current position of the Animal on
      screen
    * Change Pen Thickness - This feature allows the user to change the width of the line that is
      drawn.
    * Change Animal - This feature allows the user to replace the ImageView of an animal with a
      different Image.
    * Add Command - Users can type a command in the TextField at the bottom border pane, and then
      click Add Command to run the typed command.

* LSystem:
    * Set up Game - At the start of the game the user is prompted to insert: "length, angle, number
      of levels" for the LSystem game.
    * New Game - This feature allows the user to restart the current Darwin game to a new LSystem
      game.
    * Load Command File - This feature allows the user to choose a file from a pop-up FileChooser
      and run it as the commands for the application.
    * Set Default Position - This feature prompts the user to set the "Home" position for the turtle
    * Set Pen Color - This feature allows the user to change the pen color of the line that is drawn
    * Past Commands - This feature contains a list of all of the previous commands that have been
      run in Logo. Users can click and rerun these commands.
    * Set Background Color - This feature allows the user to change the background color of the game
      display rectangle.
    * Add Animal Stamp - This feature allows the user to choose an image to stamp on the LSystem
      drawing at its current position.
    * Add Command - Users can type a command in the TextField at the bottom border pane, and then
      click Add Command to run the typed command.

### Notes/Assumptions

Assumptions or Simplifications:

There are few areas to note with this project:

1. When running the LSystemGameView test, the user will be prompted to enter in a comma-separated
   string of parameters for a game. This needs to be manually inputted. This simplification was made
   due to difficulties in detecting the text input dialog pop-up at the start of the game rather
   than when it was triggered by a button press. For ease of use, insert "10,120,1" into the text
   field (without quotes).
2. Some errors are handled with no display, rather the action is not taken. We felt this was also an
   accurate way to handle errors because sometimes an error is thrown because a user tries to change
   a feature but then decides against it. Thus for some features such as "Change Animal" in Logo
   or "Add Animal" in LSystem, if the user simply clicks X on the pop-up nothing happens.

Interesting data files:

* logo/four_square.txt : cool usage of the tell command!
* lsystem/dragon_curve.txt : unique L-System fractal to make interesting designs
* darwin/infector.txt : infects animals quickly

Known Bugs:

There are a few bugs when it comes to error handling. First, if a user does not input a string of
parameters to the LSystem pop-up, the game will start with null parameters. The error that displays
will tell the user to start a new game. If the user tries to press the new game button on the scene
after this, a stack trace is printed. This is because the application tries to add duplicate nodes
to the group. If this error comes up at any point in the game (whether it be for LSystem or Darwin
or Logo), the user should press the red square to quit the application and then rerun it. Given time
constraints it was hard to debug this since it is necessary to have parameters for LSystem to even
initialize properly in the first place. If given more time this is one of the first things we would
fix. With LSystem also, if an illegal instruction is typed to the text field before any legal
instructions are added or loaded through a file, a stack trace is printed. If you then try to add a
valid command or load a valid file, it may return an error display, but the commands will run
correctly. Just like our other issue we knew this one was a time intensive debugging problem, and
ultimately we had to sacrifice it to improve design and general functionality more. Finally, with
Darwin sometimes an error is not displayed, rather the invalid commands or files are ignored. We did
try to catch and display errors, but at times they are caught and then the game moves on and can be
continued by a valid command insert.

Extra credit:

We added instructions to be displayed on the game screen for ease of use in playing the application.
We also implemented an animation for each game, and not just Darwin.
We attempted MVC design prior to learning this in class.
We implemented reflection to create our Commands, replacing the traditional if-else hierarchy.

### Impressions

This project was definitely quite difficult, but we were able to improve our design skills by
utilizing the principles learned in class. We used inheritance and abstract classes extensively, and
even implemented a view - controller - model design with reflection though this has not been
discussed yet. It was really exciting for us when after implementing Logo for the Basic deadline, we
were able to easily add LSystem with not too many issues. This increased our confidence that our
program was easily extensible. We also had a good separation of view and model, as we were able to
add features to each without having to change very much at all of the other. Overall, we were able
to practice all of the design principles we learned in class to create a functional project with
consideration of good design.

