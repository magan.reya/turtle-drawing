package oolala.view;

import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import oolala.controller.GameController;
import oolala.model.GameModel;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import util.DukeApplicationTest;

import java.io.FileNotFoundException;
import java.util.ResourceBundle;

public class LogoGameViewTest extends DukeApplicationTest {
    private GameView view;
    private GameController controller;
    private GameModel model;
    private Labeled label;
    private TextInputControl textField;
    public static final int SIZE = 600;
    public static Paint DEFAULT_BACKGROUND = Color.THISTLE;
    public static final String TITLE = "OOLALA";
    public static final String BUTTON_DISPLAY_RESOURCE_BUNDLE = "oolala.view.resources.ButtonTextDisplayEnglish";
    public static final String RESOURCE_PACKAGE = "oolala.view.resources.";
    public static final String TITLE_DISPLAY_RESOURCE_BUNDLE = "TitleDisplayEnglish";
    public static final String INPUT_DISPLAY_RESOURCE_BUNDLE = "InputDialogTextDisplayEnglish";
    public static final String COMBO_BOX_DISPLAY_RESOURCE_BUNDLE ="oolala.view.resources.ComboTextDisplayEnglish";
    ResourceBundle comboTextDisplays;
    ResourceBundle myTextDisplays;
    ResourceBundle titleDisplays;
    ResourceBundle inputDisplays;

    @Override
    public void start (Stage stage) throws FileNotFoundException {
        String myGame = "Logo";
        view = new LogoGameView(myGame, "English");
        model = new GameModel();
        controller = new GameController(view, model);
        view.setMyController(controller);
        view.start(1);

        stage.setTitle(TITLE);
        stage.setScene(view.makeScene(SIZE, SIZE, DEFAULT_BACKGROUND));
        stage.show();
        view.start(1);


        myTextDisplays = ResourceBundle.getBundle(BUTTON_DISPLAY_RESOURCE_BUNDLE);
        titleDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + TITLE_DISPLAY_RESOURCE_BUNDLE);
        inputDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + INPUT_DISPLAY_RESOURCE_BUNDLE);
        comboTextDisplays = ResourceBundle.getBundle(COMBO_BOX_DISPLAY_RESOURCE_BUNDLE);
        textField = lookup("#InsertCommands").query();
    }

    @Test
    void testNewGame(){
        String expected = "fd 50";
        clickOn(textField).write(expected);
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(buttonone);
        Button button = lookup(myTextDisplays.getString("NewGame")).query();
        clickOn(button);
        Button buttontwo = lookup(myTextDisplays.getString("Position")).query();
        clickOn(buttontwo);
        assertEquals(getDialogMessage(), String.format(titleDisplays.getString("position"), "200", "200"));
    }

    @Test
    void testAddCommand(){
        String expected = "fd 50";
        clickOn(textField).write(expected);
        assertEquals(expected, textField.getText());
    }

    @Test
    void stampAnimal(){
        String expected = "stamp";
        String next = "fd 50";
        String last = "lt 90";
        clickOn(textField).write(expected);
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(buttonone);
        clickOn(textField).write(next);
        clickOn(buttonone);
        clickOn(textField).write(last);
        clickOn(buttonone);
        assertEquals(250, view.getMyAnimalDisplay().getAnimal(0).getY());
    }

    @Test void changeAnimal(){
        view.getMyAnimalDisplay().changeImage("elephant");
        String expected = "stamp";
        String next = "fd 50";
        String last = "lt 90";
        clickOn(textField).write(expected);
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(buttonone);
        clickOn(textField).write(next);
        clickOn(buttonone);
        clickOn(textField).write(last);
        clickOn(buttonone);
        assertEquals(250, view.getMyAnimalDisplay().getAnimal(0).getY());
    }

    @Test
    void changePenThickness(){
        view.getMyAnimalDisplay().setLineThickness(15);
        String next = "fd 50";
        String last = "lt 90";
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(textField).write(next);
        clickOn(buttonone);
        clickOn(textField).write(last);
        clickOn(buttonone);
        assertEquals(250, view.getMyAnimalDisplay().getAnimal(0).getY());
    }

    @Test
    void drawLineTest(){
        String expected = "fd 50";
        clickOn(textField).write(expected);
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(buttonone);
        assertEquals(200, view.getMyAnimalDisplay().getAnimal(0).getX());
    }

    @Test
    void drawFromFileTest(){
        String triangleFile = "fd 50 rt 120 fd 100 rt 120 fd 100 rt 120 fd 50";
        clickOn(textField).write(triangleFile);
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(buttonone);
        assertEquals(200, view.getMyAnimalDisplay().getAnimal(0).getY());
    }

    @Test
    void testAddCommandClear(){
        String start = "fd 50";
        String expected = "";
        Button button = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(textField).write(start);
        clickOn(button);
        assertEquals(expected, textField.getText());
    }

    @Test
    void testAddInvalidCommand(){
        String expected = "fdd";
        String error= "Please input a valid instruction. The following instruction isn't valid: %s";
        Button button = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(textField).write(expected);
        clickOn(button);
        assertEquals(String.format(error, expected), getDialogMessage());
    }

    @Test
    void testSetDefaultPosition(){
        String home = "home";
        String pos = "200,200";
        Button button = lookup(myTextDisplays.getString("SetBeginningPosition")).query();
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(button);
        writeInputsToDialog(pos);
        clickOn(textField).write(home);
        clickOn(buttonone);
        Button buttontwo = lookup(myTextDisplays.getString("Position")).query();
        clickOn(buttontwo);
        assertEquals(getDialogMessage(), String.format(titleDisplays.getString("position"), "200", "200"));
    }
    @Test
    void testHistory(){
        String expected = "fd 50";
        Button button = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(textField).write(expected);
        clickOn(button);
        ComboBox<String> cb = lookup("#COMBO").query();
        select(cb, expected);
        assertEquals(expected, cb.getValue());
    }

    @Test
    void testHistoryFail(){
        String expected = "fd 50";
        String falseCmd = "fd 60";
        Button button = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(textField).write(expected);
        clickOn(button);
        ComboBox<String> cb = lookup("#COMBO").query();
        select(cb, falseCmd);
        assertNotEquals(expected, cb.getValue());
    }

    @Test
    void testPenWidthChange(){
        Button button = lookup(myTextDisplays.getString("PenThickness")).query();
        clickOn(button);
        writeInputsToDialog("y");
        assertEquals(getDialogMessage(), "Insert a numerical value");

    }

}
