package oolala.view;

import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import oolala.controller.GameController;
import oolala.model.GameModel;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import util.DukeApplicationTest;

import java.io.FileNotFoundException;
import java.util.ResourceBundle;

public class DarwinGameViewTest extends DukeApplicationTest {
    private GameView view;
    private GameController controller;
    private GameModel model;
    private Labeled label;
    private TextInputControl textField;
    public static final int SIZE = 600;
    public static Paint DEFAULT_BACKGROUND = Color.THISTLE;
    public static final String TITLE = "OOLALA";
    public static final String BUTTON_DISPLAY_RESOURCE_BUNDLE = "oolala.view.resources.ButtonTextDisplayEnglish";
    public static final String RESOURCE_PACKAGE = "oolala.view.resources.";
    public static final String TITLE_DISPLAY_RESOURCE_BUNDLE = "TitleDisplayEnglish";
    public static final String INPUT_DISPLAY_RESOURCE_BUNDLE = "InputDialogTextDisplayEnglish";
    public static final String COMBO_BOX_DISPLAY_RESOURCE_BUNDLE ="oolala.view.resources.ComboTextDisplayEnglish";
    ResourceBundle comboTextDisplays;
    ResourceBundle myTextDisplays;
    ResourceBundle titleDisplays;
    ResourceBundle inputDisplays;

    @Override
    public void start (Stage stage) throws FileNotFoundException {
        String myGame = "Darwin";
        view = new DarwinGameView(myGame, "English");
        model = new GameModel();
        controller = new GameController(view, model);
        view.setMyController(controller);
        view.start(1);

        stage.setTitle(TITLE);
        stage.setScene(view.makeScene(SIZE, SIZE, DEFAULT_BACKGROUND));
        stage.show();
        view.start(1);


        myTextDisplays = ResourceBundle.getBundle(BUTTON_DISPLAY_RESOURCE_BUNDLE);
        titleDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + TITLE_DISPLAY_RESOURCE_BUNDLE);
        inputDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + INPUT_DISPLAY_RESOURCE_BUNDLE);
        comboTextDisplays = ResourceBundle.getBundle(COMBO_BOX_DISPLAY_RESOURCE_BUNDLE);

    }

    @Test
    void testSlider(){
        Slider slider = lookup("#SLIDE").query();
        setValue(slider, 50);
        assertEquals(50, slider.getValue());
    }

    @Test
    void testAddSpecies(){
        Button button = lookup(myTextDisplays.getString("Species")).query();
        clickOn(button);
    }


}
