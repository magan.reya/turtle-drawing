package oolala.view;

import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import oolala.controller.GameController;
import oolala.model.GameModel;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import util.DukeApplicationTest;

import java.io.FileNotFoundException;
import java.util.ResourceBundle;

public class LSystemGameViewTest extends DukeApplicationTest {
    private GameView view;
    private GameController controller;
    private GameModel model;
    private Labeled label;
    private TextInputControl textField;
    public static final int SIZE = 600;
    public static Paint DEFAULT_BACKGROUND = Color.THISTLE;
    public static final String TITLE = "OOLALA";
    public static final String BUTTON_DISPLAY_RESOURCE_BUNDLE = "oolala.view.resources.ButtonTextDisplayEnglish";
    public static final String RESOURCE_PACKAGE = "oolala.view.resources.";
    public static final String TITLE_DISPLAY_RESOURCE_BUNDLE = "TitleDisplayEnglish";
    public static final String INPUT_DISPLAY_RESOURCE_BUNDLE = "InputDialogTextDisplayEnglish";
    public static final String COMBO_BOX_DISPLAY_RESOURCE_BUNDLE ="oolala.view.resources.ComboTextDisplayEnglish";
    ResourceBundle comboTextDisplays;
    ResourceBundle myTextDisplays;
    ResourceBundle titleDisplays;
    ResourceBundle inputDisplays;

    @Override
    public void start (Stage stage) throws FileNotFoundException {
        String myGame = "LSystem";
        view = new LSystemGameView(myGame, "English");
        model = new GameModel();
        controller = new GameController(view, model);

        view.setMyController(controller);


        stage.setTitle(TITLE);
        stage.setScene(view.makeScene(SIZE, SIZE, DEFAULT_BACKGROUND));
        stage.show();
        view.start(0.25);


        myTextDisplays = ResourceBundle.getBundle(BUTTON_DISPLAY_RESOURCE_BUNDLE);
        titleDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + TITLE_DISPLAY_RESOURCE_BUNDLE);
        inputDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + INPUT_DISPLAY_RESOURCE_BUNDLE);
        comboTextDisplays = ResourceBundle.getBundle(COMBO_BOX_DISPLAY_RESOURCE_BUNDLE);
        textField = lookup("#InsertCommands").query();
    }

    @Test
    void testAddCommand(){
        String expected = "START F rule F F+F-F-F+F";
        clickOn(textField).write(expected);
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(buttonone);
        assertEquals(200, view.getMyAnimalDisplay().getAnimal(0).getY());
    }

    @Test
    void testAddInvalidCommand(){
        String expected = "START F";
        clickOn(textField).write(expected);
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(buttonone);
        assertEquals(getDialogMessage(), "Need a start command and at least one rule command for L-system.");
    }

    @Test
    void addAnimalTest(){
        String expected = "START F rule F F+F-F-F+F";
        clickOn(textField).write(expected);
        Button buttonone = lookup(myTextDisplays.getString("AddCommand")).query();
        clickOn(buttonone);
        view.getMyAnimalDisplay().addImageToDrawing("leaf");
        clickOn(buttonone);
        clickOn(buttonone);
        clickOn(buttonone);
        assertEquals(200, view.getMyAnimalDisplay().getAnimal(0).getY());
    }
}

