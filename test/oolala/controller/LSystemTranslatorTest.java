package oolala.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import oolala.controller.translator.LSystemTranslator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LSystemTranslatorTest {
  ResourceBundle errors = ResourceBundle.getBundle("oolala.controller.resources.ErrorsEnglish");

  @Test
  void translateLinesOneRuleVariableTest() {
    LSystemTranslator t = new LSystemTranslator("10", "90", "2");
    List<String> inputCmds = new ArrayList<>(List.of("start F", "rule F F+F"));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(
        List.of("pu", "ht", "home", "fd 0", "rt 90", "pd fd 10", "pu", "home", "fd 70", "rt 90",
            "pd fd 10", "rt 90", "pd fd 10", "pu"), t.translateLinesIntoLogoIsns(inputCmds));
  }
  @Test
  void checkLineLengthTranslationTest() {
    LSystemTranslator t = new LSystemTranslator("13", "90", "1");
    t.setMyErrorsBundle(errors);
    List<String> inputCmds = new ArrayList<>(List.of("start F", "rule F"));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(
        "13", t.translateLinesIntoLogoIsns(inputCmds).get(5).split(" ")[2]);
  }
  @Test
  void translateLinesAddRuleTest() {
    LSystemTranslator t = new LSystemTranslator("13", "90", "2");
    List<String> inputCmds = new ArrayList<>(List.of("start T", "rule T F-G-G", "set T \"pd fd LENGTH\""));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(
        List.of("pu", "ht", "home", "fd 0", "rt 90", "pd fd 13", "pu", "home", "fd 70", "rt 90",
            "pd fd 13", "lt 90", "pu fd 13", "lt 90",  "pu fd 13", "pu"), t.translateLinesIntoLogoIsns(inputCmds));
  }

  @Test
  void checkCorrectNumLevelsGenerated() {
    LSystemTranslator t = new LSystemTranslator("13", "90", "8");
    t.setMyErrorsBundle(errors);
    List<String> inputCmds = new ArrayList<>(List.of("start F", "rule F+F+F"));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(
        8, Collections.frequency(t.translateLinesIntoLogoIsns(inputCmds), "home"));
  }

  @Test
  void checkErrorRecordedIfInvalidCharacter() {
    LSystemTranslator t = new LSystemTranslator("13", "90", "8");
    t.setMyErrorsBundle(errors);
    List<String> inputCmds = new ArrayList<>(List.of("start Z", "rule F+F+F"));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(true, t.getMyErrorHandler().isErrorFound());
    assertEquals("Illegal instruction input for L-System.", t.getMyErrorHandler().getErrorMessage());
  }
  @Test
  void longStringOfIsnsTest(){
    LSystemTranslator t = new LSystemTranslator("10", "90", "2");
    List<String> inputCmds = new ArrayList<>(List.of("start F rule F F-B set B \"pd fd 3\""));
    assertEquals(
        List.of("pu", "ht", "home", "fd 0", "rt 90", "pd fd 10", "pu", "home", "fd 70", "rt 90",
            "pd fd 10", "lt 90", "pd fd 3", "pu"), t.translateLinesIntoLogoIsns(inputCmds));

  }
  @Test
  void notEnoughCommandsPassedTest(){
    LSystemTranslator t = new LSystemTranslator("10", "90", "2");
    t.setMyErrorsBundle(errors);
    List<String> inputCmds = new ArrayList<>(List.of("start F set B \"pd fd 3\""));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(true, t.getMyErrorHandler().isErrorFound());
    assertEquals("Need a start command and at least one rule command for L-system.", t.getMyErrorHandler().getErrorMessage());

  }

  @Test
  void invalidInstructionPassed(){
    LSystemTranslator t = new LSystemTranslator("10", "90", "2");
    t.setMyErrorsBundle(errors);
    List<String> inputCmds = new ArrayList<>(List.of("start F rule F Y+F"));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(true, t.getMyErrorHandler().isErrorFound());
    assertEquals("Illegal instruction input for L-System.", t.getMyErrorHandler().getErrorMessage());

  }


}