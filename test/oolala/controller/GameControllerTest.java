package oolala.controller;


import oolala.controller.translator.DarwinTranslator;
import oolala.model.DarwinManager;
import oolala.model.GameModel;
import oolala.view.GameView;
import oolala.view.LSystemGameView;
import oolala.view.LogoGameView;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class GameControllerTest {

  @Test
  void setHomeTest() {
    GameView view = new LogoGameView("Logo", "English");
    GameModel model = new GameModel();
    GameController g = new GameController(view, model);
    g.setMyClassesForGameType("Logo");
    g.setHomeWithNewValues("300, 40");
    g.resetScene();
    assertEquals(300, model.getMyAnimalManager().getAnimals().get(0).getMyAnimalData().getMyX());
    assertEquals(40, model.getMyAnimalManager().getAnimals().get(0).getMyAnimalData().getMyY());
  }

  @Test
  void resetScene() {
    GameView view = new LogoGameView("Logo", "English");
    GameModel model = new GameModel();
    GameController g = new GameController(view, model);
    g.setMyClassesForGameType("Logo");
    g.resetScene();
    assertEquals(1, model.getMyAnimalManager().getAnimals().size());
  }

  @Test
  void listOfLogoCommandsTranslation() {
    GameView view = new LogoGameView("Logo", "English");
    GameModel model = new GameModel();
    GameController g = new GameController(view, model);
    g.setMyClassesForGameType("Logo");
    List<String> cmd = new ArrayList<>();
    cmd.add("fd 50 rt 90 pd");
    List<String> ret = g.createListOfIsnsFromInput(cmd);
    assertEquals(List.of("fd 50", "rt 90", "pd"), ret);
  }

  @Test
  void poorlyFormattedLogoCommandInputTst() {
    GameView view = new LogoGameView("Logo", "English");
    GameModel model = new GameModel();
    GameController g = new GameController(view, model);
    g.setMyClassesForGameType("Logo");
    List<String> cmd = new ArrayList<>();
    cmd.add("fd 50rt 90     pd");
    List<String> ret = g.createListOfIsnsFromInput(cmd);
    assertEquals(List.of("fd 50", "rt 90", "pd"), ret);
  }

  @Test
  void loadFromFileTest() {
    GameView view = new LogoGameView("Logo", "English");
    GameModel model = new GameModel();
    GameController g = new GameController(view, model);
    g.setMyClassesForGameType("Logo");
    List<String> ret = g.createListOfIsnsFromFile("data/examples/logo/triangle.txt");
    assertEquals(List.of("fd 50", "rt 120", "fd 100", "rt 120", "fd 100", "rt 120", "fd 50"), ret);

  }
  @Test
  void setMyClassesForGameTypeDarwin() {
    GameView view = new LSystemGameView("Darwin", "English");
    GameModel model = new GameModel();
    GameController g = new GameController(view, model);
    g.setMyClassesForGameType("Darwin");
    assertEquals(g.getMyCommandTranslator().getClass(), (new DarwinTranslator()).getClass());
    assertEquals(model.getMyAnimalManager().getClass(), (new DarwinManager().getClass()));
  }


}
