package oolala.controller;

import oolala.controller.translator.CommandTranslator;
import oolala.controller.translator.LogoTranslator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class CommandTranslatorTest {

    @Test
    void addedInputTest(){
        CommandTranslator t = new LogoTranslator();
        String commands = "fd 50 lt 90 fd 50";
        List<String> trueCommands = new ArrayList<>();
        trueCommands.add("fd 50");
        trueCommands.add("lt 90");
        trueCommands.add("fd 50");
        List<String> returnedCommands = t.translateFromLinesToIsns(List.of(commands));
        assertEquals(trueCommands, returnedCommands);
    }

    @Test
    void addedInputTestFails(){
        try {
            CommandTranslator t = new LogoTranslator();
            String commands = "fd 50 f ";
            t.translateFromLinesToIsns(List.of(commands));
        }
        catch (Exception e){
            assert(true);
        }
    }
}
