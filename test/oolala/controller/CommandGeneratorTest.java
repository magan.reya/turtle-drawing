package oolala.controller;

import oolala.commands.*;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class CommandGeneratorTest {
  ResourceBundle errors = ResourceBundle.getBundle("oolala.controller.resources.ErrorsEnglish");

  @Test
  void illegalCommandInputTest() {
    CommandGenerator g = new CommandGenerator();
    g.setMyErrorsBundle(errors);
    ArrayList<String> cmds = new ArrayList<>();
    cmds.add("fdad");
    Queue ret = g.createCommands(cmds);
    assertEquals(true, ret.isEmpty());
  }

  @Test
  void illegalValueInputTest() {
    CommandGenerator g = new CommandGenerator();
    g.setMyErrorsBundle(errors);
    ArrayList<String> cmds = new ArrayList<>();
    cmds.add("fd hj");
    g.createCommands(cmds);
    assertEquals(true, g.getMyErrorHandler().isErrorFound());
  }

  @Test
  void cmdIfValueMissingTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("bk"));
    Command testCmd = (Command) ret.remove();
    assertEquals(0, testCmd.getMyValue());
  }

  @Test
  void commandValueSetTest() {
    CommandGenerator g = new CommandGenerator();
    ArrayList<String> cmds = new ArrayList<>();
    cmds.add("fd 20");
    Queue ret = g.createCommands(cmds);
    Command cmd = (Command) ret.remove();
    assertEquals(20, cmd.getMyValue());
  }


  @Test
  void createMoveCommandForwardTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("fd 20"));
    MoveForwardCommand test = new MoveForwardCommand();
    test.setMyValue(20);
    assertEquals(test, ret.remove());
  }

  @Test
  void createMoveCommandBackwardTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("bk 20"));
    MoveBackwardCommand test = new MoveBackwardCommand();
    test.setMyValue(20);
    assertEquals(test, ret.remove());
  }

  @Test
  void createTurnLeftTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("lt 20"));
    TurnLeftCommand test = new TurnLeftCommand();
    test.setMyValue(20);
    assertEquals(test, ret.remove());
  }

  @Test
  void createTurnRightTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("rt 20"));
    TurnRightCommand test = new TurnRightCommand();
    test.setMyValue(20);
    assertEquals(test, ret.remove());
  }

  @Test
  void createHomeTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("home"));
    ResetPositionCommand test = new ResetPositionCommand();
    assertEquals(test, ret.remove());
  }

  @Test
  void createPenDownTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("pd"));
    PenDownCommand test = new PenDownCommand();
    assertEquals(test, ret.remove());
  }

  @Test
  void createPenUpTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("pu"));
    PenUpCommand test = new PenUpCommand();
    assertEquals(test, ret.remove());
  }

  @Test
  void createHideTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("ht"));
    HideCommand test = new HideCommand();
    assertEquals(test, ret.remove());
  }

  @Test
  void createShowTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("st"));
    ShowCommand test = new ShowCommand();
    assertEquals(test, ret.remove());
  }

  @Test
  void createStampTest() {
    CommandGenerator g = new CommandGenerator();
    Queue ret = g.createCommands(List.of("stamp"));
    StampCommand test = new StampCommand();
    assertEquals(test, ret.remove());
  }

  @Test
  void illegalInstructionTest() {
    CommandGenerator g = new CommandGenerator();
    g.getMyErrorHandler().setErrorFound("s");
    assertEquals(g.getMyErrorHandler().getErrorMessage(), "s");
  }

  @Test
  void generateBadCommandTest() {
    CommandGenerator g = new CommandGenerator();
    g.setMyErrorsBundle(errors);
    g.createCommands(List.of("f"));
    assertEquals(true, g.getMyErrorHandler().isErrorFound());
  }

}