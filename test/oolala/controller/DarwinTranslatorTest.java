package oolala.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import oolala.controller.translator.DarwinTranslator;
import org.junit.jupiter.api.Test;

class DarwinTranslatorTest {

  @Test
  void translateLinesIntoLogoIsns() {
    DarwinTranslator t = new DarwinTranslator();
    List<String> inputCmds = new ArrayList<>(List.of("move 5", "left 30"));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(
        List.of("fd 5", "lt 30"), t.translateLinesIntoLogoIsns(inputCmds));
  }
  @Test
  void translateUniqueDarwinIsns() {
    DarwinTranslator t = new DarwinTranslator();
    List<String> inputCmds = new ArrayList<>(List.of("move 5", "left 30", "ifempty 3"));
    t.translateLinesIntoLogoIsns(inputCmds);
    assertEquals(
        List.of("fd 5", "lt 30", "ifempty 3"), t.translateLinesIntoLogoIsns(inputCmds));
  }
  }
