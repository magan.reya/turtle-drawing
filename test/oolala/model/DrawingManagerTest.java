package oolala.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.commands.Command;
import oolala.commands.MoveBackwardCommand;
import oolala.commands.MoveForwardCommand;
import oolala.commands.PenUpCommand;
import oolala.commands.TellCommand;
import oolala.commands.TurnRightCommand;
import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class DrawingManagerTest {

  @Test
  void autoCreateAnimalsInADrawingGame() {
    DrawingManager dm = new DrawingManager();
    DrawingAnimal animal = new DrawingAnimal(0);
    animal.getMyAnimalData().setMyType("turtle");
    assertEquals(List.of(animal),dm.getAnimals());
  }

  @Test
  void addCommandsToSingleAnimalTest() {
    DrawingManager dm = new DrawingManager();
    Queue<Command> inputQueue = new LinkedList<>(List.of(new MoveBackwardCommand(), new MoveForwardCommand(), new TurnRightCommand(), new PenUpCommand()));
    dm.addCommands(inputQueue);
    assertEquals(inputQueue, dm.getAnimals().get(0).getCommandsToExecute());
  }

  @Test
  void addCommandsToMultipleAnimalTest() {
    DrawingManager dm = new DrawingManager();
    TellCommand tellAdd = new TellCommand();
    tellAdd.setMyValue(1);
    dm.addCommands(new LinkedList<>(List.of(tellAdd)));
    dm.executeCommand();
    Queue<Command> inputQueue = new LinkedList<>(List.of(new MoveBackwardCommand(), new MoveForwardCommand(), new TurnRightCommand(), new PenUpCommand()));
    dm.addCommands(inputQueue);
    assertEquals(inputQueue, dm.getAnimals().get(0).getCommandsToExecute());
    assertEquals(inputQueue, dm.getAnimals().get(1).getCommandsToExecute());
  }

  @Test
  void executeCommandOnAllAnimalsTest() {
    DrawingManager dm = new DrawingManager();
    TellCommand tellAdd = new TellCommand();
    tellAdd.setMyValue(1);
    dm.addCommands(new LinkedList<>(List.of(tellAdd)));
    dm.executeCommand();
    Queue<Command> inputQueue = new LinkedList<>(List.of(new MoveBackwardCommand(), new MoveForwardCommand(), new TurnRightCommand(), new PenUpCommand()));
    dm.addCommands(inputQueue);
    dm.executeCommand();
    inputQueue.remove();
    assertEquals(inputQueue, dm.getAnimals().get(0).getCommandsToExecute());
    assertEquals(inputQueue, dm.getAnimals().get(1).getCommandsToExecute());
  }

  @Test
  void onlyOneAnimalForNewGameTest() {
    DrawingManager dm = new DrawingManager();
    TellCommand tellAdd = new TellCommand();
    tellAdd.setMyValue(1);
    dm.addCommands(new LinkedList<>(List.of(tellAdd)));
    dm.executeCommand();
    Queue<Command> inputQueue = new LinkedList<>(List.of(new MoveBackwardCommand(), new MoveForwardCommand(), new TurnRightCommand(), new PenUpCommand()));
    dm.addCommands(inputQueue);
    dm.clearOutForNewGame();
    assertEquals(1, dm.getAnimals().size());
  }
  @Test
  void animalHasNoCommandsInNewGame() {
    DrawingManager dm = new DrawingManager();
    Queue<Command> inputQueue = new LinkedList<>(List.of(new MoveBackwardCommand(), new MoveForwardCommand(), new TurnRightCommand(), new PenUpCommand()));
    dm.addCommands(inputQueue);
    dm.clearOutForNewGame();
    assertEquals(new LinkedList<Command>(), dm.getAnimals().get(0).getCommandsToExecute());
  }
  @Test
  void animalIsResetToOriginalPosition() {
    DrawingManager dm = new DrawingManager();
    MoveBackwardCommand mb = new MoveBackwardCommand();
    mb.setMyValue(30);
    Queue<Command> inputQueue = new LinkedList<>(List.of(mb));
    dm.addCommands(inputQueue);
    dm.clearOutForNewGame();
    assertEquals(200, dm.getAnimals().get(0).getMyAnimalData().getMyX());
  }
}