package oolala.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.commands.Command;
import oolala.commands.GoCommand;
import oolala.commands.InfectCommand;
import oolala.commands.MoveBackwardCommand;
import oolala.commands.MoveForwardCommand;
import oolala.commands.PenUpCommand;
import oolala.commands.TurnRightCommand;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;
import org.junit.jupiter.api.Test;

class DarwinManagerTest {

  @Test
  void createAnimalsForGameSizeTest() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    assertEquals(10, dm.getAnimals().size());
  }

  @Test
  void myCurrentSpeciesAfterAnimalCreationTest() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    assertEquals(dm.getMyCurrentSpecies(), "turtle");
  }

  @Test
  void correctIDAssignmentTest() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    dm.createAnimalsForGame("falcon");
    assertEquals(10, dm.getAnimals().get(10).getMyAnimalData().getMyID());
  }

  @Test
  void addingMultipleSpecies() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    dm.createAnimalsForGame("falcon");
    assertEquals(20, dm.getAnimals().size());
  }

  @Test
  void addCommandsToMostRecentlyInputtedSpeciesTest() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    dm.createAnimalsForGame("falcon");
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveBackwardCommand(), new MoveForwardCommand(), new TurnRightCommand(),
            new PenUpCommand()));
    dm.addCommands(inputQueue);
    assertEquals(inputQueue, dm.getAnimals().get(13).getCommandsToExecute());
    assertEquals(new LinkedList<>(), dm.getAnimals().get(0).getCommandsToExecute());
  }

  @Test
  void addCommandsAllAnimalsOfASpecies() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    dm.createAnimalsForGame("falcon");
    Queue<Command> inputQueue = new LinkedList<>(
        List.of( new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand()));
    dm.addCommands(inputQueue);
    assertEquals(inputQueue, dm.getAnimals().get(13).getCommandsToExecute());
    assertEquals(inputQueue, dm.getAnimals().get(17).getCommandsToExecute());
  }
  @Test
  void executeCommandOneSpeciesTest() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    Queue<Command> inputQueueTurtle = new LinkedList<>(
        List.of( new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand(), new GoCommand()));
    dm.addCommands(inputQueueTurtle);
    dm.executeCommand();
    assertEquals(inputQueueTurtle, dm.getAnimals().get(8).getCommandsToExecute());
    DarwinAnimal da  = (DarwinAnimal) dm.getAnimals().get(1);
    assertEquals(2, da.getCmdCurrentIndex());

  }
  @Test
  void executeCommandMaxIndexTest() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    Queue<Command> inputQueueTurtle = new LinkedList<>(
        List.of( new MoveForwardCommand(), new InfectCommand()));
    dm.addCommands(inputQueueTurtle);
    dm.executeCommand();
    dm.executeCommand();
    dm.executeCommand();
    DarwinAnimal da  = (DarwinAnimal) dm.getAnimals().get(1);
    assertEquals(2, da.getCmdCurrentIndex());

  }

  @Test
  void executeCommandTwoSpeciesTest() {
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("turtle");
    Queue<Command> inputQueueTurtle = new LinkedList<>(
        List.of( new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand(), new GoCommand()));
    dm.addCommands(inputQueueTurtle);
    dm.createAnimalsForGame("falcon");
    Queue<Command> inputQueueFalcon = new LinkedList<>(
        List.of( new MoveForwardCommand(), new GoCommand()));
    dm.addCommands(inputQueueFalcon);
    dm.executeCommand();
    assertEquals(inputQueueTurtle, dm.getAnimals().get(3).getCommandsToExecute());
    assertEquals(inputQueueFalcon, dm.getAnimals().get(17).getCommandsToExecute());

  }

  @Test
  void clearOutForNewGame() {
  }
}