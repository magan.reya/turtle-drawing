package oolala.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.commands.Command;
import oolala.commands.InfectCommand;
import oolala.commands.MoveForwardCommand;
import oolala.commands.TurnRightCommand;
import oolala.model.DarwinAnimal;
import org.junit.jupiter.api.Test;

class DarwinAnimalTest {

  @Test
  void getNextCommandSimpleTest() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    Queue<Command> inputQueue = new LinkedList<>(
        List.of( new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand()));
    da.addCommands(inputQueue);
    assertEquals(new MoveForwardCommand(), da.getNextCommand());
  }
  @Test
  void getNextCommandCheckSizeTest() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand()));
    da.addCommands(inputQueue);
    da.getNextCommand();
    da.getNextCommand();
    assertEquals(3, da.getCommandsToExecute().size());
  }
  @Test
  void getNextCommandOutOfBoundsTest() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand()));
    da.addCommands(inputQueue);
    da.getNextCommand();
    da.getNextCommand();
    da.getNextCommand();
    da.getNextCommand();
    assertEquals(new TurnRightCommand(), da.getNextCommand());
  }

  @Test
  void getCommandAtParticularIndexTest() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand()));
    da.addCommands(inputQueue);
    da.setCmdCurrentIndex(3);
    assertEquals(new InfectCommand(), da.getNextCommand());
  }

  @Test
  void checkCommandIndexAfterAdding() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand()));
    da.addCommands(inputQueue);
    assertEquals(1, da.getCmdCurrentIndex());
  }
  @Test
  void infectChangeSpeciesTest() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    DarwinAnimal victim = new DarwinAnimal(1, "squirrel");
    da.infect(List.of(victim));
    assertEquals("falcon", victim.getMyAnimalData().getMyType());
  }
  @Test
  void checkTransferOfCommandsInInfect() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    DarwinAnimal victim = new DarwinAnimal(1, "squirrel");
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveForwardCommand(), new TurnRightCommand(),
            new InfectCommand()));
    da.addCommands(inputQueue);
    da.infect(List.of(victim));
    assertEquals(da.getCommandsToExecute(), victim.getCommandsToExecute());
  }

  @Test
  void multipleVictimAnimalsTest() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    DarwinAnimal victim = new DarwinAnimal(1, "squirrel");
    DarwinAnimal victim2 = new DarwinAnimal(2, "beaver");
    da.infect(List.of(victim, victim2));
    assertEquals("falcon", victim.getMyAnimalData().getMyType());
    assertEquals("falcon", victim2.getMyAnimalData().getMyType());
  }
  @Test
  void wallNearMeTest() {
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    da.getMyAnimalData().setMyX(399);
    assertEquals(true, da.wallNearMe(da.getMyAnimalData().getMyX(), da.getMyAnimalData().getMyY()));
  }

  @Test
  void moveWithWallNearMeTest(){
    DarwinAnimal da = new DarwinAnimal(0, "falcon");
    da.getMyAnimalData().setMyX(390);
    da.move(50);
    assertEquals(250, da.getMyAnimalData().getMyY());

  }
}