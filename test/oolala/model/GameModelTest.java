package oolala.model;

import oolala.controller.GameController;
import oolala.view.GameView;
import oolala.view.LogoGameView;
import org.junit.jupiter.api.Test;


import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GameModelTest {

   @Test
    void outOfInstructionsNullTest(){
        GameModel g = new GameModel();
        assertTrue(g.outOfInstructions());
    }

    @Test
    void outOfInstructionsFalseTest(){
        GameModel g = new GameModel();
        String myGame = "Logo";
        GameView v = new LogoGameView(myGame, "English");
        GameController controller = new GameController(v, g);
        g.setupAnimals("turtle");
        String inst = "fd 50";
        controller.createCommands(List.of(inst));
        assertEquals(false, g.outOfInstructions());
    }

}
