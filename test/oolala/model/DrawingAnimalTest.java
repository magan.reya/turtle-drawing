package oolala.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.commands.Command;
import oolala.commands.MoveForwardCommand;
import oolala.commands.PenUpCommand;
import oolala.commands.ShowCommand;
import oolala.commands.TurnRightCommand;
import oolala.model.DrawingAnimal;
import org.junit.jupiter.api.Test;

class DrawingAnimalTest {

  @Test
  void getNextCommandSimpleTest() {
    DrawingAnimal da = new DrawingAnimal(0);
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveForwardCommand(), new TurnRightCommand(),
            new PenUpCommand()));
    da.addCommands(inputQueue);
    assertEquals(new MoveForwardCommand(), da.getNextCommand());
  }

  @Test
  void getNextCommandRemoveTest() {
    DrawingAnimal da = new DrawingAnimal(0);
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveForwardCommand(), new TurnRightCommand(),
            new PenUpCommand()));
    da.addCommands(inputQueue);
    da.getNextCommand();
    assertEquals(2, da.getCommandsToExecute().size());
  }

  @Test
  void replicateClassTest() {
    DrawingAnimal da = new DrawingAnimal(0);
    DrawingAnimal replicated = da.replicate(1);
    assertEquals(da.getClass(), replicated.getClass());
  }

  @Test
  void replicateIDTest() {
    DrawingAnimal da = new DrawingAnimal(0);
    DrawingAnimal replicated = da.replicate(1);
    assertEquals(1, replicated.getMyAnimalData().getMyID());
  }
  @Test
  void setCommandToExecuteTest() {
    DrawingAnimal da = new DrawingAnimal(0);
    da.addCommands(new LinkedList<>(List.of(new ShowCommand())));
    Queue<Command> inputQueue = new LinkedList<>(
        List.of(new MoveForwardCommand(), new TurnRightCommand(),
            new PenUpCommand()));
    da.clearCommands();
    da.setCommandsToExecute(inputQueue);
    assertEquals(inputQueue, da.getCommandsToExecute());
  }

}
