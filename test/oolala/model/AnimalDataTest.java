package oolala.model;

import oolala.model.Animal;
import oolala.model.AnimalData;
import oolala.model.DrawingAnimal;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AnimalDataTest {
    @Test
    void setMyXTest(){
        AnimalData ad = new AnimalData(0);
        ad.setMyX(30);
        assertEquals(30, ad.getMyX());
    }
    @Test
    void setMyXWithAnimalResetTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        ad.setMyX(30);
        a.resetPosition();
        assertEquals(200, ad.getMyX());
    }

    @Test
    void setMyYTest(){
        AnimalData ad = new AnimalData(0);
        ad.setMyY(30);
        assertEquals(30, ad.getMyY());
    }

    @Test
    void setMyYWithAnimalChangeTest(){
        Animal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        ad.setMyY(30);
        a.resetPosition();
        assertEquals(200, ad.getMyY());
    }

    @Test
    void moveForwardTest(){
        Animal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.move(50);
        assertEquals(250, Math.round(ad.getMyY()));
    }

    @Test
    void moveBackTest(){
        Animal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.move(-50);
        assertEquals(150, Math.round(ad.getMyY()));
    }

    @Test
    void rotateClockwiseAndMoveForwardTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.rotate(90);
        a.move(50);
        assertEquals(150, Math.round(ad.getMyX()));
    }

    @Test
    void rotateClockwiseAndMoveBackwardTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.rotate(90);
        a.move(-50);
        assertEquals(250, Math.round(ad.getMyX()));
    }

    @Test
    void rotateCounterClockwiseAndMoveForwardTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.rotate(-90);
        a.move(50);
        assertEquals(250, Math.round(ad.getMyX()));
    }
    @Test
    void rotateCounterClockwiseAndMoveBackwardTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.rotate(-90);
        a.move(-50);
        assertEquals(150, Math.round(ad.getMyX()));
    }
    @Test
    void setStampTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.stampAnimal();
        assertTrue(ad.getStamp());
    }
    @Test
    void setStampOffTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.stampAnimal();
        ad.getStamp();
        assertFalse(ad.getStamp());
    }


    @Test
    void showAnimalTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.showAnimal();
        assertEquals(ad.getShow(), true);
    }

    @Test
    void hideAnimalTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.hideAnimal();
        assertEquals(ad.getShow(), false);
    }

    @Test
    void penDownTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.penDown();
        assertTrue(ad.getPenStatus());
    }

    @Test
    void penUpTest(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        a.penUp();
        assertFalse(ad.getPenStatus());
    }

    @Test
    void  setMyType(){
        DrawingAnimal a = new DrawingAnimal(0);
        AnimalData ad = a.getMyAnimalData();
        ad.setMyType("Otter");
        assertEquals("Otter", ad.getMyType());
    }

    @Test
    void  checkIDTest(){
        DrawingAnimal a = new DrawingAnimal(5);
        AnimalData ad = a.getMyAnimalData();
        assertEquals(5,  ad.getMyID());
    }


}
