package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;
import org.junit.jupiter.api.Test;

class IfEmptyCommandTest {

  @Test
  void singleAnimalCheckIfEmptyTest() {
    IfEmptyCommand cmd = new IfEmptyCommand();
    cmd.setMyValue(3);
    DarwinAnimal a = new DarwinAnimal(0, "bear");
    DarwinManager dm = new DarwinManager();
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of(cmd, new InfectCommand(), new MoveBackwardCommand(), new MoveForwardCommand()));
    a.addCommands(inputQueue);
    cmd.executeInstruction(a, dm);
    assertEquals(3, a.getCmdCurrentIndex());
  }

  @Test
  void multipleAnimalsFarApartTest() {
    IfEmptyCommand cmd = new IfEmptyCommand();
    cmd.setMyValue(4);
    DarwinAnimal a = new DarwinAnimal(0, "bear");
    DarwinAnimal b = new DarwinAnimal(1, "toucan");
    DarwinManager dm = new DarwinManager();
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of( cmd, new InfectCommand(), new MoveBackwardCommand(), new MoveForwardCommand()));
    a.addCommands(inputQueue);
    cmd.executeInstruction(a, dm);
    assertEquals(4, a.getCmdCurrentIndex());
  }
}