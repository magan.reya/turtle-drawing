package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;
import org.junit.jupiter.api.Test;

class IfRandomCommandTest {

  @Test
  void choosingRandomNextIndexTest() {
    IfRandomCommand cmd = new IfRandomCommand();
    cmd.setMyValue(3);
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("bear");
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of( cmd, new MoveBackwardCommand(), new InfectCommand(),new MoveForwardCommand()));
    dm.addCommands(inputQueue);
    dm.executeCommand();
    DarwinAnimal a = (DarwinAnimal) dm.getAnimals().get(0);
    boolean eitherIsn = (a.getCmdCurrentIndex() == 3 || a.getCmdCurrentIndex() == 4);
    assertEquals(true, eitherIsn);
  }

  @Test
  void badRandomValudNextIndexTest() {
    IfRandomCommand cmd = new IfRandomCommand();
    cmd.setMyValue(40);
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("bear");
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of( cmd, new MoveBackwardCommand(), new InfectCommand(),new MoveForwardCommand()));
    dm.addCommands(inputQueue);
    dm.executeCommand();
    DarwinAnimal a = (DarwinAnimal) dm.getAnimals().get(0);
    boolean eitherIsn = (a.getCmdCurrentIndex() == 3 || a.getCmdCurrentIndex() == 1);
    assertEquals(true, eitherIsn);
  }
}