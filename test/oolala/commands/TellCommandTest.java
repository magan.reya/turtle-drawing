package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class TellCommandTest {

  @Test
  void createAnotherAnimalTest() {
   TellCommand cmd = new TellCommand();
    cmd.setMyValue(40);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(dm.getAnimals().get(0), dm);
    assertEquals(2, dm.getAnimals().size());
  }
  @Test
  void checkThatAnimalsHaveSameIsns() {
    TellCommand cmd = new TellCommand();
    cmd.setMyValue(40);
    DrawingManager dm = new DrawingManager();
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of(cmd, new MoveForwardCommand()));
    dm.addCommands(inputQueue);
    cmd.executeInstruction(dm.getAnimals().get(0), dm);
    assertEquals(dm.getAnimals().get(0).getCommandsToExecute(), dm.getAnimals().get(1).getCommandsToExecute());
  }
  @Test
  void checkThatNewAnimalHasCorrectID() {
    TellCommand cmd = new TellCommand();
    cmd.setMyValue(40);
    DrawingManager dm = new DrawingManager();
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of(cmd, new MoveForwardCommand()));
    dm.addCommands(inputQueue);
    cmd.executeInstruction(dm.getAnimals().get(0), dm);
    assertEquals(40, dm.getAnimals().get(1).getMyAnimalData().getMyID());
  }
}