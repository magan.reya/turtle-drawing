package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class ResetPositionCommandTest {

  @Test
  void resetPositionAfterMovingTest() {
    MoveForwardCommand cmd = new MoveForwardCommand();
    ResetPositionCommand reset = new ResetPositionCommand();
    cmd.setMyValue(40);
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    reset.executeInstruction(a, dm);
    assertEquals(200, a.getMyAnimalData().getMyY());
  }

  @Test
  void resetPositionToNewHome() {
    ResetPositionCommand reset = new ResetPositionCommand();
    DrawingManager dm = new DrawingManager();
    dm.createAnimalsForGame("turtle");
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of(reset));
    dm.addCommands(inputQueue);
    dm.setNewHomeForAnimals(300, 300);
    dm.executeCommand();
    DrawingAnimal a = (DrawingAnimal) dm.getAnimals().get(0);
    assertEquals(300, a.getMyAnimalData().getMyY());
  }

}