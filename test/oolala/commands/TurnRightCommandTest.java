package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class TurnRightCommandTest {

  @Test
  void rightTurnTestPostiveVal() {
    TurnRightCommand cmd = new TurnRightCommand();
    cmd.setMyValue(40);
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(130, a.getMyAnimalData().getAngle());
  }
  @Test
  void rightTurnTestNegativeVal() {
    TurnRightCommand cmd = new TurnRightCommand();
    cmd.setMyValue(-40);
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(50, a.getMyAnimalData().getAngle());
  }
  @Test
  void rightTurnTestNoVal() {
    TurnRightCommand cmd = new TurnRightCommand();
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(90, a.getMyAnimalData().getAngle());
  }
}