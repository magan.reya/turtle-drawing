package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;
import org.junit.jupiter.api.Test;

class IfEnemyCommandTest {

  @Test
  void mutlipleSpeciesIfEnemyTest() {
    IfEnemyCommand cmd = new IfEnemyCommand();
    cmd.setMyValue(3);
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("bear");
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of( cmd, new MoveBackwardCommand(), new InfectCommand(),new MoveForwardCommand()));
    dm.addCommands(inputQueue);
    dm.createAnimalsForGame("toucan");
    dm.radiusChange(300);
    dm.addCommands(inputQueue);
    DarwinAnimal da = (DarwinAnimal) dm.getAnimals().get(0);
    cmd.executeInstruction(da, dm);
    assertEquals(3, da.getCmdCurrentIndex());
  }

  @Test
  void allAnimalsOfSameTypeEnemyTest() {
    IfEnemyCommand cmd = new IfEnemyCommand();
    cmd.setMyValue(3);
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("beaver");
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of( cmd, new MoveBackwardCommand(), new InfectCommand(),new MoveForwardCommand()));
    dm.addCommands(inputQueue);
    dm.createAnimalsForGame("beaver");
    dm.radiusChange(300);
    dm.addCommands(inputQueue);
    DarwinAnimal da = (DarwinAnimal) dm.getAnimals().get(0);
    cmd.executeInstruction(da, dm);
    assertEquals(1, da.getCmdCurrentIndex());
  }
}