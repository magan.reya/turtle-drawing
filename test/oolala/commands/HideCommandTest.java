package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class HideCommandTest {

  @Test
  void hideDrawingAnimalTest() {
    HideCommand cmd = new HideCommand();
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    Queue<Command> inputQueueTurtle = new LinkedList<Command>(
        List.of(cmd));
    a.addCommands(inputQueueTurtle);
    cmd.executeInstruction(a, dm);
    assertEquals(false, a.getMyAnimalData().getShow());
  }
}