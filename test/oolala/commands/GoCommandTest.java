package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;
import org.junit.jupiter.api.Test;

class GoCommandTest {

  @Test
  void goSpecifiedIndexTest() {
    GoCommand cmd = new GoCommand();
    cmd.setMyValue(3);
    DarwinAnimal a = new DarwinAnimal(0, "bear");
    DarwinManager dm = new DarwinManager();
    Queue<Command> inputQueueTurtle = new LinkedList<Command>(
        List.of(new MoveForwardCommand(), new InfectCommand(), new MoveBackwardCommand()));
    a.addCommands(inputQueueTurtle);
    cmd.executeInstruction(a, dm);
    assertEquals(3, a.getCmdCurrentIndex());
  }
  @Test
  void goIndexBeyondSpecifiedTest() {
    GoCommand cmd = new GoCommand();
    cmd.setMyValue(40);
    DarwinAnimal a = new DarwinAnimal(0, "bear");
    DarwinManager dm = new DarwinManager();
    Queue<Command> inputQueueTurtle = new LinkedList<Command>(
        List.of(new MoveForwardCommand(), new InfectCommand(), new MoveBackwardCommand()));
    a.addCommands(inputQueueTurtle);
    cmd.executeInstruction(a, dm);
    a.getNextCommand();
    assertEquals(2, a.getCmdCurrentIndex());
  }
}