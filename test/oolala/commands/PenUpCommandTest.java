package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class PenUpCommandTest {
  @Test
  void executePenUpCommandTest() {
    PenUpCommand cmd = new PenUpCommand();
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(false, a.getMyAnimalData().getPenStatus());
  }
  @Test
  void checkPenUpAfterAnotherIsnTest() {
    PenUpCommand cmd = new PenUpCommand();
    MoveForwardCommand move = new MoveForwardCommand();
    move.setMyValue(50);
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    move.executeInstruction(a,dm);
    assertEquals(false, a.getMyAnimalData().getPenStatus());
  }
}