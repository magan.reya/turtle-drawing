package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class MoveForwardCommandTest {

  @Test
  void moveForwardPostiveVal() {
    MoveForwardCommand cmd = new MoveForwardCommand();
    cmd.setMyValue(40);
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(240, a.getMyAnimalData().getMyY());
  }
  @Test
  void moveFowardIntoWall() {
    TurnRightCommand cmd = new TurnRightCommand();
    cmd.setMyValue(500);
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(200, a.getMyAnimalData().getMyY());
  }
}