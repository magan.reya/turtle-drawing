package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class TurnLeftCommandTest {

  @Test
  void rightLeftTestPostiveVal() {
    TurnLeftCommand cmd = new TurnLeftCommand();
    cmd.setMyValue(40);
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(50, a.getMyAnimalData().getAngle());
  }
  @Test
  void rightLeftTestNegativeVal() {
    TurnLeftCommand cmd = new TurnLeftCommand();
    cmd.setMyValue(-40);
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(130, a.getMyAnimalData().getAngle());
  }
  @Test
  void rightLeftTestNoVal() {
    TurnLeftCommand cmd = new TurnLeftCommand();
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(90, a.getMyAnimalData().getAngle());
  }
}