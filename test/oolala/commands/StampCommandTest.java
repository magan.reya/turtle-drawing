package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class StampCommandTest {

  @Test
  void executeStampCommandTest() {
    StampCommand cmd = new StampCommand();
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(true, a.getMyAnimalData().getStamp());
  }
  @Test
  void checkIfStampResetsAfterCheckingTest() {
    StampCommand cmd = new StampCommand();
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    a.getMyAnimalData().getStamp();
    assertEquals(false, a.getMyAnimalData().getStamp());
  }
}