package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import oolala.model.DrawingAnimal;
import oolala.model.DrawingManager;
import org.junit.jupiter.api.Test;

class PenDownCommandTest {

  @Test
  void executePenDownCommandTest() {
    PenDownCommand cmd = new PenDownCommand();
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    assertEquals(true, a.getMyAnimalData().getPenStatus());
  }
  @Test
  void executePenDownTwiceInARowTest() {
    PenDownCommand cmd = new PenDownCommand();
    DrawingAnimal a = new DrawingAnimal(0);
    DrawingManager dm = new DrawingManager();
    cmd.executeInstruction(a, dm);
    cmd.executeInstruction(a,dm);
    assertEquals(true, a.getMyAnimalData().getPenStatus());
  }
}