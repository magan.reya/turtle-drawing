package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;
import org.junit.jupiter.api.Test;

class IfWallCommandTest {

  @Test
  void singleAnimalFarFromWallTest() {
    IfWallCommand cmd = new IfWallCommand();
    cmd.setMyValue(3);
    DarwinAnimal a = new DarwinAnimal(0, "bear");
    DarwinManager dm = new DarwinManager();
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of(cmd, new InfectCommand(), new MoveBackwardCommand(), new MoveForwardCommand()));
    a.addCommands(inputQueue);
    cmd.executeInstruction(a, dm);
    assertEquals(1, a.getCmdCurrentIndex());
  }

  @Test
  void animalMoveToWallAttemptTest() {
    IfWallCommand cmd = new IfWallCommand();
    cmd.setMyValue(5);
    MoveForwardCommand move = new MoveForwardCommand();
    move.setMyValue(180);
    DarwinAnimal a = new DarwinAnimal(0, "bear");
    DarwinManager dm = new DarwinManager();
    dm.radiusChange(40);
    move.executeInstruction(a, dm);
    cmd.executeInstruction(a, dm);
    assertEquals(1, a.getCmdCurrentIndex());
  }
}