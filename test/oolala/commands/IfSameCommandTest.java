package oolala.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import oolala.model.DarwinAnimal;
import oolala.model.DarwinManager;
import org.junit.jupiter.api.Test;

class IfSameCommandTest {

  @Test
  void smallRadiusIfSameTest() {
    IfSameCommand cmd = new IfSameCommand();
    cmd.setMyValue(3);
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("bear");
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of( cmd, new MoveBackwardCommand(), new InfectCommand(),new MoveForwardCommand()));
    dm.addCommands(inputQueue);
    dm.createAnimalsForGame("bear");
    dm.radiusChange(70);
    dm.addCommands(inputQueue);
    DarwinAnimal da = (DarwinAnimal) dm.getAnimals().get(0);
    cmd.executeInstruction(da, dm);
    assertEquals(3, da.getCmdCurrentIndex());
  }

  @Test
  void dontFindAnySameAnimalsInOriginalPositionTest() {
    IfSameCommand cmd = new IfSameCommand();
    cmd.setMyValue(3);
    DarwinManager dm = new DarwinManager();
    dm.createAnimalsForGame("beaver");
    Queue<Command> inputQueue = new LinkedList<Command>(
        List.of( cmd, new MoveBackwardCommand(), new InfectCommand(),new MoveForwardCommand()));
    dm.addCommands(inputQueue);
    dm.radiusChange(5);
    DarwinAnimal da = (DarwinAnimal) dm.getAnimals().get(0);
    cmd.executeInstruction(da, dm);
    assertEquals(1, da.getCmdCurrentIndex());
  }
}