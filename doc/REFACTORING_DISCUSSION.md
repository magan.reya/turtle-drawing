## Lab Discussion

### Team 9

### Names Kristen Angell, Reya Magan, Will Long

### Issues in Current Code

#### Method or Class

* Design issues:

Largest issue is in CommandGenerator class, where we use a long series of if statements comparing
the given string to a known string from a resource file to determine the right command to make.

* Design issue:

Error handling. Throwing the correct exceptions in the right locations. We were alerted to a lot of
issues with where exceptions are thrown and the type of exceptions that are thrown.

* Design issue:

Animal data issue, we use this object to pass information from the model to the controller to the
view to update the location of the turtle on the screen. Lots of getters and setters. To us, this is
somewhat of an issue, as we are not keeping the data completely encapsualted in the animal class.

* Design issue:

GameView depending on a lot of other classes, and is a large class, in general. Lots of methods and
difficult to implement other games easily right now.

* Design issue:

Clean up interaction between controller and view - lots of passing back and forth with buttons, etc.
Would like to find a better way to do this that seperates these two classes more. That way they
would be less dependent on each other.



### Refactoring Plan

* What are the code's biggest issues/Which issues are easy to fix and which are hard?

  * Design issues - GameView issue. Fixing would increase number of classes and split up the large
    chunks of code for each specific program. It will make it easier to implement unique methods for
    each game. This is a top priority for us right now.

  * Design issue - Error handling. Can be easily fixed we think. Need to read more about how to handle
    errors well. Think that this will make it easier to handle errors for Darwin if we get a consistet
    system.

Lesser priority:

Animal data issue -> think it would be very costly in terms of time, and not provide too much of a
benefit in the long term for our program/implementing Darwin. Command Generator class -> this is our
largest class, but the best solution would be to do reflection, which we haven't learned yet. This
will be a fix for the end if we have time.

* What are good ways to implement the changes "in place"?
  * We can start with making small changes and make sure that the code is still functional (via the tests). If the game also runs well, then we can add more design changes.

### Refactoring Work

* Issue chosen: Fix and Alternatives
  
  * To fix the view, we will make it an abstract class. We will
    then have each game type have its own class that extends this. This way, we will have a smaller abstract view class, and also make it very extendable.


* Issue chosen: Fix and Alternatives

  * To fix the error issues, we have an error handling class. We will continue to develop this class to contain any active errors and communicate them to the view via the controller.