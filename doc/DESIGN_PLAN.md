# OOLALA Design Plan

## Reya Magan, Kristen Angell, Will Long

### Team Roles and Responsibilities

* First, the team will develop the abstract classes together (with the commonalities of each application type). Then, we will
  move onto implementing each of the three different applications (with each of us taking a main role for one
  application)
* Our goal is to first implement the two drawing applications for basic, then work on extending to Darwin for complete
* We will all work on the frontend together


### High Level Design

* We will have a top-level GameModel class that holds the step loop that follows the process of each game. There will be
  one game model for all applications.
* We will have an abstract class called Animal that sets up all the common features for the animal from the three
  applications. These would include methods for moving the animal in different directions/angles, and drawing and
  marking paths. Each of the applications would extend this class to create a unique type of turtle for that
  application, and the animal class would return an Animal member myTurtle to the AnimalManager.
* Each command will be an object that will be able to execute itself. We will have an abstract Command class that we
  will extend for each command that the user can input.
* The GameController class would parse the input and create commands from it using the CommandGenerator class - the user
  would be able to input a text file with the instructions and the class will return Command objects.
* The result of parsing would a collection of Commands (maybe a queue), and it would be sent to the GameModel where the
  commands will be distributed to the TurtleManager to be executed on the turtles.
* A possible error is that a user could choose to play one game (eg Darwin) but insert instructions for another (eg
  Logo). This would give an "Invalid Instructions" error and allow the user to insert a new set of valid instructions.
* The instructions would be checked in two ways. First, we would validate that the instructions were relevant to the
  chosen application (perhaps by having an initial line in the text file designating what application it is), and then
  while parsing through each instruction we would check for any invalid ones.
* The errors would be reported using some sort of showError method that would display a pop-up that informs the user of
  the error.
* The command knows its value right when it is read in the CommandGenerator class. It will generate the correct object
  with the specified value (whether that be through a text file or user input)
* The view would communicate back to the controller, which will update the model. We will use a history list to keep
  track of all of the commands after they are executed. The GUI will then display all the previous commands using some
  sort of choice box.

### CRC Card Classes

This class's purpose or value is to display the front end of the game:

|GameView | |
|---|---|
|void play()         ||
|void setupGame()      |Main|
|void update()      ||

This class's purpose or value is to set up the game back end and run the general process for each game:

|GameModel| |
|---|---|
|void executeStep()         |AnimalManager|
|void  sendAnimalData(AnimalData)  |Controller|

This class's purpose or value is to take in commands and generate command objects from them.

|CommandGenerator| |
|---|---|
|Command createCommand(Instruction)         |Model, Animal Manager, Controller|


This class's purpose or value is to act as the communicator between the model and the view.

|GameController| |
|---|---|
|void step()         |AnimalManager, GameModel, CommandGenerator|
|void  getAnimalData()  |Model|
|void sendCommands() |GameModel
|void sendButtonPressed() |GameModel
|void createCommand() |CommandGenerator

This class's purpose or value is to serve as the main moving and interacting object in each game. We will have a
different animal for each game type that extends the abstract animal class (eg DarwinAnimal):

|Animal (abstract)| |
|---|---|
|void move()        ||
|void turn()        ||
|void display()     ||
|void affectOthers(Animal) ||
|void markSpot() ||

This class's purpose is to hold all of the data for an animal so that it can be passed around to update the view, etc.

|Animal Data| |
|---|---|
|List<int> getCurrentLocation  |Controller|
|boolean getDisplayed        |Controller|
|boolean getPenUp |Controller|

This class's purpose or value is to serve as the manager that stores all animals and directs and gives commands to
animals:

|Animal Manager()| |
|---|---|
|void getAnimal()        ||
|void addInstruction(Command)       ||
|void executeCommand(Animal)     ||
|void getAnimalData(Animal) ||

This class's purpose or value is to store and run commands for each application (each individual command will be its own
class that extends Command):

|Command (abstract)| |
|---|---|
|void executeInstruction(Animal, Animal Manager)        ||
|void setValue(int Value) |CommandGenerator|


This class's purpose or value is store all the commands to be executed and the history of executed commands:

|CommandManager| |
|---|---|
|void addNewCommand(Command)   |Model, Controller|
|Command getNextCommand() |AnimalManager|
|Command getCommandFromHistory(index) |Controller, AnimalManager|

 

### Use Cases

* The user types 'fd 50' in the command window, sees the turtle move in the display window leaving a trail, and has the
  command added to the environment's history.

```{r, tidy=FALSE, eval=FALSE, highlight=FALSE }
//Get input from text field in GameView class

String inputInstruction = getTextInput()

//Pass to controller

GameController.createCommand(inputInstruction)

//Command generator creates command, map determines which type of command string refers to -> in this case, forward command generated with value 50 

CommandGenerator.createCommand(inputInstruction)
-> 
//map.get("fd") -> new ForwardCommand()

Command currentCommand = map.get("fd")
currentCommand.setValue(50)

//send to Command Manager

GameModel.getCommandManager().addNewCommand(currentCommand)

//inside addNewCommand in Command Manager

myCommandsQueue.add(currentCommand);

//back in controller class after createCommand
step()
->
Model.executeStep()

//inside of model step
CurrentAnimal = AnimalManager.getAnimal();
executeCommand(CurrentAnimal)

//inside of executeCommand (command selected automatically added to history)
Command toExecute = myCommandManager.getNextCommand()
toExecute.executeInstruction(CurrentAnimal, this)

//inside of execute instruction, this will move animal
CurrentAnimal.move(value)
->
//AnimalData gets updated with new values

//completed instruction, now pass animal data back up to controller
AnimalData updatedData = getAnimalData(currentAnimal.getAnimalData())

//view will ask if there's new data, if yes, update the turtle's location and mark between old and new positions
View.update()
->
GameModel.getAnimalData()
    
```
* The user loads a file of commands, sees the turtle move in the display window leaving a trail, and has the command
  added to the environment's history.

```{r, tidy=FALSE, eval=FALSE, highlight=FALSE }
//Get input using button in GameView class

LoadFileButton.whenClicked(event -> 
{promptUserToInputFile()
file f = fileReadIn;
GameController.readFile()

//inside read file, loop that reads line by line and calls createCommand for each line

createCommand(inputInstruction)

//Command generator creates command, map determines which type of command string refers to -> in this case, forward command generated with value 50 

CommandGenerator.createCommand(inputInstruction)
-> 
//map.get("fd") -> new ForwardCommand()

Command currentCommand = map.get("fd")
currentCommand.setValue(50)

//send to Command Manager

GameModel.getCommandManager().addNewCommand(currentCommand)

//inside addNewCommand in Command Manager

myCommandsQueue.add(currentCommand);

//back in controller class after createCommand
step()
->
Model.executeStep()

//inside of model step
CurrentAnimal = AnimalManager.getAnimal();
executeCommand(CurrentAnimal)

//inside of executeCommand (command selected automatically added to history)
Command toExecute = myCommandManager.getNextCommand()
toExecute.executeInstruction(CurrentAnimal, this)

//inside of execute instruction, this will move animal
CurrentAnimal.move(value)
->
//AnimalData gets updated with new values

//completed instruction, now pass animal data back up to controller
AnimalData updatedData = getAnimalData(currentAnimal.getAnimalData())

//view will ask if there's new data, if yes, update the turtle's location and mark between old and new positions
View.update()
->
GameModel.getAnimalData()
```
* The user types '50 fd' in the command window and sees an error message that the command was not formatted correctly.
```{r, tidy=FALSE, eval=FALSE, highlight=FALSE }
//Get input from text field in GameView class

String inputInstruction = getTextInput()

//Pass to controller

GameController.createCommand(inputInstruction)

//Command generator creates command, map determines which type of command string refers to -> in this case, forward command generated with value 50 

CommandGenerator.createCommand(inputInstruction)
-> 
//map.get("50") -> null
 if (map.get(first part of instruction) == null) {
        showError("Invalid Instruction");
        show TextInputField box again
        }
```

* The user clicks in the display window to set a new Home location.

```java
 // user clicks New Home button

View.resetHome();
//Pops up a TextField input, once for a new x value, then for new y value
//Set instance variable myHomeX = x input, myHomeY = y input
// View resets home

```

* The user changes the color of the environment's background.

```{r, tidy=FALSE, eval=FALSE, highlight=FALSE }

//user clicks Change Background Color choice box
 
//list of potential colors pops up for user to select

myBackgroundColor = userColorChoice;
myScene.setFill(myBackgroundColor);


```

